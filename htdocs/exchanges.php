<?php
	define("ROOT_PATH",realpath("../")."/");
	$exchanges = json_decode(file_get_contents(ROOT_PATH.'data/exchanges'), TRUE);
?><!DOCTYPE html><html>
 <head>
  <link rel="stylesheet" href="css/default.css" type="text/css">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Bitcoin Markets - Exchanges</title>
 </head>
 <body>
  <table>
   <thead>
    <tr>
     <th>Exchange</th>
     <th>Website</th>
     <th>Email</th>
     <th>Freenode</th>
     <th>BitcoinTalk.org</th>
     <th>Phone</th>
     <th>Address</th>
    </tr>
   </thead>
   <tbody>
<?php foreach ($exchanges as $key => $exchange) { ?>
    <tr class="<?php echo $key % 2 == 0 ? 'odd' : 'even'; ?>">
     <td><a href="markets.php?eid=<?php echo $exchange['eid']; ?>"><?php echo $exchange['ename']; ?></a></td>
     <td><a href="<?php echo $exchange['ewebsite']; ?>"><?php echo $exchange['ewebsite']; ?></a></td>
     <td><a href="mailto:<?php echo $exchange['eemail']; ?>"><?php echo $exchange['eemail']; ?></a></td>
     <td><?php echo $exchange['efreenode']; ?></td>
     <td><a href="<?php echo "https://bitcointalk.org/index.php?action=profile;u=",$exchange['ebitcointalk']; ?>"><?php echo $exchange['ebitcointalk']; ?></a></td>
     <td><?php echo $exchange['ephone']; ?></td>
     <td><?php echo $exchange['eaddress']; ?></td>
<?php } ?>
   </tbody>
  </table>
 </body>
</html>