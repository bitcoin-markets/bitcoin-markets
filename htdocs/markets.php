<?php
	define("ROOT_PATH",realpath("../")."/");

	// http://www.php.net/manual/en/ref.bc.php#85084
	function bcround($strval, $precision = 0) {
		if (false !== ($pos = strpos($strval, '.')) && (strlen($strval) - $pos - 1) > $precision) {
			$zeros = str_repeat("0", $precision);
			return bcadd($strval, "0.{$zeros}5", $precision);
		} else return $strval;
	}

	function div($a, $b) { $c =bcdiv($a, $b, 12); if (strpos($c, '.') === FALSE) return $c; return rtrim(rtrim($c, '0'), '.'); }

	function hunum($n) {
		if (strpos($n, '.') === FALSE) return $n;
		$int = FALSE; $o = "";
		for ($x=-1; $x>=-strlen($n); $x--) {
			if ($int !== FALSE) {
				if ($int++ == 3) { $o = "," . $o; $int = 1; }
			}
			$i = substr($n, $x, 1);
			$o = $i . $o;
			if ($i === '.') $int = 0;
		}
		return $o;
	}

	function mul($a, $b) { $c =bcmul($a, $b, 12); if (strpos($c, '.') === FALSE) return $c; return rtrim(rtrim($c, '0'), '.'); }

	function perc($f) { return bcdiv(round(bcmul($f, '10000', 12)), '100', 2).'%'; }

	function sub($a, $b) { $c =bcsub($a, $b, 12); if (strpos($c, '.') === FALSE) return $c; return rtrim(rtrim($c, '0'), '.'); }

	$currencies = array();
	$data = json_decode(file_get_contents(ROOT_PATH.'data/currencies'), TRUE);
	foreach ($data as $currency) $currencies[$currency['cid']] = array('cabbr' => $currency['cabbr'], 'cname' => $currency['cname']);

	$exchanges = array();
	$data = json_decode(file_get_contents(ROOT_PATH.'data/exchanges'), TRUE);
	foreach ($data as $exchange) $exchanges[$exchange['eid']] = array(
		'ename' => $exchange['ename'],
		'ewebsite' => $exchange['ewebsite'],
		'eemail' => $exchange['eemail'],
		'efreenode' => $exchange['efreenode'],
		'ebitcointalk' => $exchange['ebitcointalk'],
		'ephone' => $exchange['ephone'],
		'eaddress' => $exchange['eaddress'],
		'edevname' => $exchange['edevname']);

	$markets = array();
	$data = json_decode(file_get_contents(ROOT_PATH.'data/markets'), TRUE);
	foreach ($data as $market) {
		$include = TRUE;
		if (isset($_GET['eid']) && $_GET['eid'] != $market['mexchange_id']) $include = FALSE;
		if (isset($_GET['currency']) && $_GET['currency'] != $currencies[$market['mcurrency_id']]['cabbr']) $include = FALSE;
		if ($include)
			$markets[$market['mid']] = array(
				'mid' => $market['mid'], // Needed for sorting
				'mexchange_id' => $market['mexchange_id'],
				'msymbol' => $market['msymbol'],
				'mcurrency_id' => $market['mcurrency_id'],
				'mwebsite' => $market['mwebsite'],
				'mdata_depth' => $market['mdata_depth'],
				'mdata_depth_all' => $market['mdata_depth_all'],
				'mdata_ticker' => $market['mdata_ticker'],
				'mdata_trades' => $market['mdata_trades'],
				'mdata_trades_all' => $market['mdata_trades_all'],
				'mdata_trades_canceled' => $market['mdata_trades_canceled'],
				'mactive' => $market['mactive'],
				'mtransactionfee' => $market['mtransactionfee'],
				'mtype' => $market['mtype']);
	}

	$trades = array();
	$data = json_decode(file_get_contents(ROOT_PATH.'data/trades'), TRUE);
	foreach ($data as $trade) {
		$trades[$trade['mid']] = array();
		if (isset($trade['latest_trade_id'])) {
			$trades[$trade['mid']]['latest_trade_id'] = $trade['latest_trade_id'];
			$trades[$trade['mid']]['latest_trade_date'] = $trade['latest_trade_date'];
			$trades[$trade['mid']]['latest_trade_price'] = rtrim(rtrim($trade['latest_trade_price'], '0'), '.');
			$trades[$trade['mid']]['latest_trade_quantity'] = $trade['latest_trade_quantity'];
		} else {
			$trades[$trade['mid']]['latest_trade_id'] = '';
			$trades[$trade['mid']]['latest_trade_date'] = '';
			$trades[$trade['mid']]['latest_trade_price'] = '';
			$trades[$trade['mid']]['latest_trade_quantity'] = '';
		}
		if (isset($trade['previousclose_trade_id'])) {
			$trades[$trade['mid']]['previousclose_trade_id'] = $trade['previousclose_trade_id'];
			$trades[$trade['mid']]['previousclose_trade_date'] = $trade['previousclose_trade_date'];
			$trades[$trade['mid']]['previousclose_trade_price'] = rtrim(rtrim($trade['previousclose_trade_price'], '0'), '.');
			$trades[$trade['mid']]['previousclose_trade_quantity'] = $trade['previousclose_trade_quantity'];
		} else {
			$trades[$trade['mid']]['previousclose_trade_id'] = '';
			$trades[$trade['mid']]['previousclose_trade_date'] = '';
			$trades[$trade['mid']]['previousclose_trade_price'] = '';
			$trades[$trade['mid']]['previousclose_trade_quantity'] = '';
		}
		if (isset($trade['open_trade_id'])) {
			$trades[$trade['mid']]['open_trade_id'] = $trade['open_trade_id'];
			$trades[$trade['mid']]['open_trade_date'] = $trade['open_trade_date'];
			$trades[$trade['mid']]['open_trade_price'] = rtrim(rtrim($trade['open_trade_price'], '0'), '.');
			$trades[$trade['mid']]['open_trade_quantity'] = $trade['open_trade_quantity'];
		} else {
			$trades[$trade['mid']]['open_trade_id'] = '';
			$trades[$trade['mid']]['open_trade_date'] = '';
			$trades[$trade['mid']]['open_trade_price'] = '';
			$trades[$trade['mid']]['open_trade_quantity'] = '';
		}
		if (isset($trade['highbid']))
			$trades[$trade['mid']]['highbid'] = rtrim(rtrim($trade['highbid'], '0'), '.');
		else
			$trades[$trade['mid']]['highbid'] = '';
		if (isset($trade['lowask']))
			$trades[$trade['mid']]['lowask'] = rtrim(rtrim($trade['lowask'], '0'), '.');
		else
			$trades[$trade['mid']]['lowask'] = '';
		if (isset($trade['dayhigh']))
			$trades[$trade['mid']]['dayhigh'] = rtrim(rtrim($trade['dayhigh'], '0'), '.');
		else
			$trades[$trade['mid']]['dayhigh'] = '';
		if (isset($trade['daylow']))
			$trades[$trade['mid']]['daylow'] = rtrim(rtrim($trade['daylow'], '0'), '.');
		else
			$trades[$trade['mid']]['daylow'] = '';

		$trades[$trade['mid']]['previousclose_pricediff'] = '';
		$trades[$trade['mid']]['previousclose_pricediffperc'] = '';
		if (isset($trade['latest_trade_price'])) {
			if ($trade['previousclose_trade_price'] == $trade['latest_trade_price']) {
				$trades[$trade['mid']]['previousclose_pricediff'] = '0';
				$trades[$trade['mid']]['previousclose_pricediffperc'] = '0.00%';
			} else {
				if ($trade['previousclose_trade_price']) {
					$trades[$trade['mid']]['previousclose_pricediff'] = sub($trade['latest_trade_price'], $trade['previousclose_trade_price']);
					$trades[$trade['mid']]['previousclose_pricediffperc'] = perc(div($trades[$trade['mid']]['previousclose_pricediff'], $trade['previousclose_trade_price']));
				} else {
					$trades[$trade['mid']]['previousclose_pricediff'] = '';
					$trades[$trade['mid']]['previousclose_pricediffperc'] = '';
				}
				if ($trade['previousclose_trade_price'] < $trade['latest_trade_price'] && $trades[$trade['mid']]['previousclose_pricediff'])
					$trades[$trade['mid']]['previousclose_pricediff'] = '+' . $trades[$trade['mid']]['previousclose_pricediff'];
			}
		}

		if (isset($trade['todayvolumequantity'])) {
			$trades[$trade['mid']]['todayvolumequantity'] = bcmul($trade['todayvolumequantity'], '1', 2);
			$trades[$trade['mid']]['todayvolumeprice'] = bcmul($trade['todayvolumeprice'], '1', 2);
			$trades[$trade['mid']]['todayvolumepriceavg'] = bcmul($trade['todayvolumepriceavg'], '1', 8);
		}
		if (isset($trade['dailyvolumequantity'])) {
			$trades[$trade['mid']]['dailyvolumequantity'] = bcmul($trade['dailyvolumequantity'], '1', 2);
			$trades[$trade['mid']]['dailyvolumeprice'] = bcmul($trade['dailyvolumeprice'], '1', 2);
			$trades[$trade['mid']]['dailyvolumepriceavg'] = bcmul($trade['dailyvolumepriceavg'], '1', 8);
		}
		if (isset($trade['monthlyvolumequantity'])) {
			$trades[$trade['mid']]['monthlyvolumequantity'] = bcmul($trade['monthlyvolumequantity'], '1', 2);
			$trades[$trade['mid']]['monthlyvolumeprice'] = bcmul($trade['monthlyvolumeprice'], '1', 2);
		}

		//if (!isset($_SESSION['lastdata'])) $_SESSION['lastdata'] = array();
		//if (!isset($_SESSION['lastdata'][$markets[$trade['mid']]['msymbol']])) $_SESSION['lastdata'][$markets[$trade['mid']]['msymbol']] = array(
		//	'dailyvolumequantity' => $trade['dailyvolumequantity'],
		//	'daylow' => $trade['daylow'],
		//	'dayhigh' => $trade['dayhigh'],
		//	'latest_trade_date' => $trade['latest_trade_date']
		//);
	}

	$sorts = array(
		'ask' => 'lowask',
		'bid' => 'highbid',
		'currency' => 'cabbr',
		'todayvolume' => 'todayvolumequantity',
		'todayvwap' => 'today',
		'todayavg' => 'todayavg',
		'dailyvolume' => 'dailyvolumequantity',
		'dailyvwap' => 'daily',
		'dailyavg' => 'dailyavg',
		'latestprice' => 'latest_trade_date',
		'monthlyvolume' => 'monthlyvolumequantity',
		'monthlyvwap' => 'monthly',
		'open' => 'open_trade_price',
		'previousclose' => 'previousclose_trade_price',
		'symbol' => 'msymbol',
		'transactionfee' => 'mtransactionfee',
		'type' => 'mtype');
	$sort = 'monthlyvolume'; $sortorder = 'DESC';
	if (isset($_GET['sort']) && array_key_exists($_GET['sort'], $sorts)) $sort = $_GET['sort'];
	if (isset($_GET['sortorder']) && in_array($_GET['sortorder'], array('ASC', 'DESC'))) $sortorder = $_GET['sortorder'];

	function cmp($a, $b) {
		global $sort, $sortorder, $sorts;
		if ($sortorder == 'DESC') $ord = -1; else $ord = 1;
		if (in_array($sort, array('ask', 'bid', 'dailyvolume', 'latestprice', 'monthlyvolume', 'open', 'previousclose', 'todayvolume'))) {
			global $trades;
			if ($trades[$a['mid']][$sorts[$sort]] == $trades[$b['mid']][$sorts[$sort]]) return 0;
			if ($trades[$a['mid']][$sorts[$sort]] == "") return 1;
			if ($trades[$b['mid']][$sorts[$sort]] == "") return -1;
			return (($trades[$a['mid']][$sorts[$sort]] < $trades[$b['mid']][$sorts[$sort]]) ? -1 : 1) * $ord;
		} else if (in_array($sort, array('dailyvwap', 'monthlyvwap', 'todayvwap'))) {
			global $trades;
			if (bcdiv($trades[$a['mid']][$sorts[$sort].'volumeprice'], $trades[$a['mid']][$sorts[$sort].'volumequantity'], 8)
				== bcdiv($trades[$b['mid']][$sorts[$sort].'volumeprice'], $trades[$b['mid']][$sorts[$sort].'volumequantity'], 8)) return 0;
			if ($trades[$a['mid']][$sorts[$sort].'volumequantity'] == "") return 1;
			if ($trades[$b['mid']][$sorts[$sort].'volumequantity'] == "") return -1;
			return ((bcdiv($trades[$a['mid']][$sorts[$sort].'volumeprice'], $trades[$a['mid']][$sorts[$sort].'volumequantity'], 8)
				< bcdiv($trades[$b['mid']][$sorts[$sort].'volumeprice'], $trades[$b['mid']][$sorts[$sort].'volumequantity'], 8)) ? -1 : 1) * $ord;
		} else if (in_array($sort, array('currency'))) {
			global $currencies;
			if ($currencies[$a['mcurrency_id']][$sorts[$sort]] == $currencies[$b['mcurrency_id']][$sorts[$sort]]) return 0;
			if ($currencies[$a['mcurrency_id']][$sorts[$sort]] == "") return 1;
			if ($currencies[$b['mcurrency_id']][$sorts[$sort]] == "") return -1;
			return (($currencies[$a['mcurrency_id']][$sorts[$sort]] < $currencies[$b['mcurrency_id']][$sorts[$sort]]) ? -1 : 1) * $ord;
		} else {
			if ($a[$sorts[$sort]] == $b[$sorts[$sort]]) return 0;
			if ($a[$sorts[$sort]] == "") return 1;
			if ($b[$sorts[$sort]] == "") return -1;
			return (($a[$sorts[$sort]] < $b[$sorts[$sort]]) ? -1 : 1) * $ord;
		}
	}

	usort($markets, 'cmp');
?><!DOCTYPE html><html>
 <head>
  <link rel="stylesheet" href="css/default.css" type="text/css">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Bitcoin Markets - Markets</title>
 </head>
 <body>
  <table>
   <thead>
    <tr>
     <th><a href="<?php echo
	'markets.php?sort=symbol&amp;sortorder=',
	($sort != 'symbol' || $sortorder != 'ASC' ? 'ASC' : 'DESC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">Symbol</a></th>
     <th><a href="<?php echo
	'markets.php?sort=currency&amp;sortorder=',
	($sort != 'currency' || $sortorder != 'ASC' ? 'ASC' : 'DESC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">Currency</a></th>
     <th><a href="<?php echo
	'markets.php?sort=type&amp;sortorder=',
	($sort != 'type' || $sortorder != 'ASC' ? 'ASC' : 'DESC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">Type</a></th>
     <th><a href="<?php echo
	'markets.php?sort=transactionfee&amp;sortorder=',
	($sort != 'transactionfee' || $sortorder != 'ASC' ? 'ASC' : 'DESC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">Transaction Fee</a></th>
     <th><a href="<?php echo
	'markets.php?sort=latestprice&amp;sortorder=',
	($sort != 'latestprice' || $sortorder != 'DESC' ? 'DESC' : 'ASC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">Latest Price</a></th>
     <th><a href="<?php echo
	'markets.php?sort=previousclose&amp;sortorder=',
	($sort != 'previousclose' || $sortorder != 'ASC' ? 'ASC' : 'DESC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">Previous Close</a></th>
     <th><a href="<?php echo
	'markets.php?sort=todayvolume&amp;sortorder=',
	($sort != 'todayvolume' || $sortorder != 'DESC' ? 'DESC' : 'ASC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">Today Volume</a></th>
     <th><a href="<?php echo
	'markets.php?sort=todayvwap&amp;sortorder=',
	($sort != 'todayvwap' || $sortorder != 'DESC' ? 'DESC' : 'ASC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">Today VWAP</a></th>
     <th><a href="<?php echo
	'markets.php?sort=todayavg&amp;sortorder=',
	($sort != 'todayavg' || $sortorder != 'DESC' ? 'DESC' : 'ASC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">Today Avg Price</a></th>
     <th><a href="<?php echo
	'markets.php?sort=dailyvolume&amp;sortorder=',
	($sort != 'dailyvolume' || $sortorder != 'DESC' ? 'DESC' : 'ASC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">24h Volume</a></th>
     <th><a href="<?php echo
	'markets.php?sort=dailyvwap&amp;sortorder=',
	($sort != 'dailyvwap' || $sortorder != 'DESC' ? 'DESC' : 'ASC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">24h VWAP</a></th>
     <th><a href="<?php echo
	'markets.php?sort=dailyavg&amp;sortorder=',
	($sort != 'dailyavg' || $sortorder != 'DESC' ? 'DESC' : 'ASC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">24h Avg Price</a></th>
     <th><a href="<?php echo
	'markets.php?sort=monthlyvolume&amp;sortorder=',
	($sort != 'monthlyvolume' || $sortorder != 'DESC' ? 'DESC' : 'ASC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">30d Volume</a></th>
     <th><a href="<?php echo
	'markets.php?sort=monthlyvwap&amp;sortorder=',
	($sort != 'monthlyvwap' || $sortorder != 'DESC' ? 'DESC' : 'ASC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">30d VWAP</a></th>
     <th>Day Low/High</th>
     <th><a href="<?php echo
	'markets.php?sort=open&amp;sortorder=',
	($sort != 'open' || $sortorder != 'ASC' ? 'ASC' : 'DESC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">Open</a></th>
     <th><a href="<?php echo
	'markets.php?sort=bid&amp;sortorder=',
	($sort != 'bid' || $sortorder != 'DESC' ? 'DESC' : 'ASC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">Bid</a></th>
     <th><a href="<?php echo
	'markets.php?sort=ask&amp;sortorder=',
	($sort != 'ask' || $sortorder != 'ASC' ? 'ASC' : 'DESC'),
	(isset($_GET['currency']) ? '&currency=' . $_GET['currency'] : ''),
	(isset($_GET['eid']) ? '&eid=' . $_GET['eid'] : '');
	?>">Ask</a></th>
    </tr>
   </thead>
   <tbody>
<?php foreach ($markets as $mid => $market) { if ($market['mactive'] == TRUE) { ?>
    <tr class="<?php echo $mid % 2 == 0 ? 'odd' : 'even'; ?>">
     <td><?php echo $market['msymbol']; ?></td>
     <td><?php echo $currencies[$market['mcurrency_id']]['cabbr']; ?></td>
     <td><?php echo $market['mtype']; ?></td>
     <td><?php echo $market['mtransactionfee']; ?></td>
     <td class="nowrap"><?php if ($trades[$market['mid']]['latest_trade_price'] != '') { echo $trades[$market['mid']]['latest_trade_price'],' (',date('M j Y, H:i:s', $trades[$market['mid']]['latest_trade_date']),')'; } ?></td>
     <td class="nowrap"><?php if ($trades[$market['mid']]['previousclose_trade_price'] != '') { echo $trades[$market['mid']]['previousclose_trade_price'],' (',$trades[$market['mid']]['previousclose_pricediff'],' ',$trades[$market['mid']]['previousclose_pricediffperc'],')'; } ?></td>
     <td class="nowrap"><?php echo $trades[$market['mid']]['todayvolumequantity'],($trades[$market['mid']]['todayvolumeprice'] ? ' ('.$trades[$market['mid']]['todayvolumeprice'].' '.$currencies[$market['mcurrency_id']]['cabbr'].')' : ''); ?></td>
     <td class="nowrap"><?php echo bcdiv($trades[$market['mid']]['todayvolumeprice'], $trades[$market['mid']]['todayvolumequantity'], 8); ?></td>
     <td class="nowrap"><?php echo $trades[$market['mid']]['todayvolumepriceavg']; ?></td>
     <td class="nowrap"><?php echo $trades[$market['mid']]['dailyvolumequantity'],($trades[$market['mid']]['dailyvolumeprice'] ? ' ('.$trades[$market['mid']]['dailyvolumeprice'].' '.$currencies[$market['mcurrency_id']]['cabbr'].')' : ''); ?></td>
     <td class="nowrap"><?php echo bcdiv($trades[$market['mid']]['dailyvolumeprice'], $trades[$market['mid']]['dailyvolumequantity'], 8); ?></td>
     <td class="nowrap"><?php echo $trades[$market['mid']]['dailyvolumepriceavg']; ?></td>
     <td class="nowrap"><?php echo hunum($trades[$market['mid']]['monthlyvolumequantity']),($trades[$market['mid']]['monthlyvolumeprice'] ? ' ('.hunum($trades[$market['mid']]['monthlyvolumeprice']).' '.$currencies[$market['mcurrency_id']]['cabbr'].')' : ''); ?></td>
     <td class="nowrap"><?php echo bcdiv($trades[$market['mid']]['monthlyvolumeprice'], $trades[$market['mid']]['monthlyvolumequantity'], 8); ?></td>
     <td><?php if ($trades[$market['mid']]['daylow'] || $trades[$market['mid']]['dayhigh']) { echo $trades[$market['mid']]['daylow'],'/',$trades[$market['mid']]['dayhigh']; } ?></td>
     <td><?php echo $trades[$market['mid']]['open_trade_price']; ?></td>
     <td><?php echo $trades[$market['mid']]['highbid']; ?></td>
     <td><?php echo $trades[$market['mid']]['lowask']; ?></td>
    </tr>
<?php } } ?>
   </tbody>
  </table>
 </body>
</html>