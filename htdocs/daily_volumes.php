<?php
	define("ROOT_PATH",realpath("../")."/");

	// http://www.php.net/manual/en/ref.bc.php#85084
	function bcround($strval, $precision = 0) {
		if (false !== ($pos = strpos($strval, '.')) && (strlen($strval) - $pos - 1) > $precision) {
			$zeros = str_repeat("0", $precision);
			return bcadd($strval, "0.{$zeros}5", $precision);
		} else return $strval;
	}

	function div($a, $b) { $c =bcdiv($a, $b, 12); if (strpos($c, '.') === FALSE) return $c; return rtrim(rtrim($c, '0'), '.'); }

	function hunum($n) { if ($n >= 1) return number_format(floor($n)); return rtrim($n, '0'); }

	function mul($a, $b) { $c =bcmul($a, $b, 12); if (strpos($c, '.') === FALSE) return $c; return rtrim(rtrim($c, '0'), '.'); }

	function perc($f) { return bcdiv(round(bcmul($f, '10000', 12)), '100', 2).'%'; }

	function sub($a, $b) { $c =bcsub($a, $b, 12); if (strpos($c, '.') === FALSE) return $c; return rtrim(rtrim($c, '0'), '.'); }

	$currencies = array();
	$data = json_decode(file_get_contents(ROOT_PATH.'data/currencies'), TRUE);
	foreach ($data as $currency) $currencies[$currency['cid']] = array('cabbr' => $currency['cabbr'], 'cname' => $currency['cname']);

	$exchanges = array();
	$data = json_decode(file_get_contents(ROOT_PATH.'data/exchanges'), TRUE);
	foreach ($data as $exchange) $exchanges[$exchange['eid']] = array(
		'ename' => $exchange['ename'],
		'ewebsite' => $exchange['ewebsite'],
		'eemail' => $exchange['eemail'],
		'efreenode' => $exchange['efreenode'],
		'ebitcointalk' => $exchange['ebitcointalk'],
		'ephone' => $exchange['ephone'],
		'eaddress' => $exchange['eaddress'],
		'edevname' => $exchange['edevname']);

	$markets = array();
	$data = json_decode(file_get_contents(ROOT_PATH.'data/markets'), TRUE);
	foreach ($data as $market) {
		$include = TRUE;
		if (isset($_GET['eid']) && $_GET['eid'] != $market['mexchange_id']) $include = FALSE;
		if (isset($_GET['currency']) && $_GET['currency'] != $currencies[$market['mcurrency_id']]['cabbr']) $include = FALSE;
		if ($include)
			$markets[$market['mid']] = array(
				'mid' => $market['mid'],
				'mexchange_id' => $market['mexchange_id'],
				'msymbol' => $market['msymbol'],
				'mcurrency_id' => $market['mcurrency_id'],
				'mactive' => $market['mactive'],
				'mtype' => $market['mtype'],
				'hasdata' => FALSE);
	}

	$daily_volumes = array(); $total_volumes = array();
	$data = json_decode(file_get_contents(ROOT_PATH.'data/daily_volumes'), TRUE);
	foreach ($data as $daily_volume) {
		if (!isset($daily_volumes[$daily_volume['day']])) $daily_volumes[$daily_volume['day']] = array();
		if (!isset($total_volumes[$daily_volume['mid']])) $total_volumes[$daily_volume['mid']] = $daily_volume['volume'];
		else $total_volumes[$daily_volume['mid']] += $daily_volume['volume'];
		$daily_volumes[$daily_volume['day']][$daily_volume['mid']] = array('volume' => $daily_volume['volume'], 'spent' => $daily_volume['spent']);
		$markets[$daily_volume['mid']]['hasdata'] = TRUE;
	}

	$data = json_decode(file_get_contents(ROOT_PATH.'data/trades'), TRUE);
	foreach ($data as $trade) {
		if ($trade['todayvolumedate'] != '') {
			if (!isset($daily_volumes[$trade['todayvolumedate']])) $daily_volumes[$trade['todayvolumedate']] = array();
			$daily_volumes[$trade['todayvolumedate']][$trade['mid']] = array('volume' => $trade['todayvolumequantity'], 'spent' => $trade['todayvolumeprice']);
		}
	}
	$daily_volumes = array_reverse($daily_volumes, TRUE);

	$sorts = array('alpha', 'totalvolume', 'volume'); $sort = 'volume';
	if (isset($_GET['sort']) && in_array($_GET['sort'], $sorts)) $sort = $_GET['sort'];

	function sortmarkets($a, $b) {
		global $sort;
		if ($sort == 'alpha') { if ($a['msymbol'] == $b['msymbol']) return 0; return ($a['msymbol'] < $b['msymbol']) ? -1 : 1; }
		if ($sort == 'totalvolume') {
			global $total_volumes;
			if ($total_volumes[$a['mid']] == $total_volumes[$b['mid']]) return 0;
			return ($total_volumes[$a['mid']] < $total_volumes[$b['mid']]) ? 1 : -1;
		} if ($sort == 'volume') {
			global $daily_volumes;
			$volumes = current($daily_volumes);
			if ($volumes[$a['mid']]['volume'] == $volumes[$b['mid']]['volume']) return 0;
			return ($volumes[$a['mid']]['volume'] < $volumes[$b['mid']]['volume']) ? 1 : -1;
		}
	}
	uasort($markets, "sortmarkets");
?><!DOCTYPE html><html>
 <head>
  <link rel="stylesheet" href="css/default.css" type="text/css">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Bitcoin Markets - Daily Trade Volumes</title>
 </head>
 <body>
  <table>
   <thead>
    <tr>
     <th>date</th>
<?php foreach ($markets as $mid => $market) { if ($market['mactive'] == TRUE && $market['hasdata'] == TRUE) { ?>
     <th><?php echo $market['msymbol']; ?></th>
<?php } } ?>
    </tr>
   </thead>
   <tbody>
    <tr class="dark">
     <td>Total</td>
<?php	foreach ($markets as $mid => $market) { if ($market['mactive'] == TRUE && $market['hasdata'] == TRUE) { ?>
     <td><?php echo hunum($total_volumes[$mid]); ?></td>
<?php 	} } ?>
    </tr>
<?php foreach ($daily_volumes as $ts => $dv_markets) { ?>
    <tr class="<?php echo $ts % 172800 != 0 ? 'odd' : 'even'; ?>">
     <td class="nowrap"><?php echo date("M d Y", $ts); ?></td>
<?php	foreach ($markets as $mid => $market) { if ($market['mactive'] == TRUE && $market['hasdata'] == TRUE) { ?>
     <td><?php if (isset($dv_markets[$mid]) && $dv_markets[$mid]['volume'] != "0E-12") echo hunum($dv_markets[$mid]['volume']); ?></td>
<?php 	} } ?>
    </tr>
<?php } ?>
   </tbody>
  </table>
 </body>
</html>