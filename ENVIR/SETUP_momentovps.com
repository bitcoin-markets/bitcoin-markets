boot gentoo livecd

configure network

configure partitions
	# parted /dev/sda
		mklabel gpt
		mkpart primary ext2 1MB 32MB
		name 1 boot
		set 1 boot on
		mkpart primary ext3 32.5MB 288.5MB
		name 2 root
		mkpart primary linux-swap 288.5MB 4384.5MB
		name 3 swap
		mkpart primary ext4 4384.5MB -1
		name 4 lvm
		set 4 lvm on
		quit

	# mkfs.ext2 /dev/sda1
	# mkfs.ext4 /dev/sda2
	# mkswap /dev/sda3
	# swapon /dev/sda3
	# mount /dev/sda2 /mnt/gentoo
	# mkdir /mnt/gentoo/boot
	# mount /dev/sda1 /mnt/gentoo/boot

	# pvcreate /dev/sda4
	# vgcreate btcm /dev/sda4

	# lvcreate -L4G -nusr btcm
	# mkfs.ext4 /dev/btcm/usr
	# mkdir /mnt/gentoo/usr
	# mount /dev/btcm/usr /mnt/gentoo/usr

	# lvcreate -L4G -nhome btcm
	# mkfs.ext4 /dev/btcm/home
	# mkdir /mnt/gentoo/home
	# mount /dev/btcm/home /mnt/gentoo/home

	# lvcreate -L1G -nopt btcm
	# mkfs.ext4 /dev/btcm/opt
	# mkdir /mnt/gentoo/opt
	# mount /dev/btcm/opt /mnt/gentoo/opt

	# lvcreate -L4G -nportage btcm
	# mkfs.ext4 /dev/btcm/portage
	# mkdir /mnt/gentoo/usr/portage
	# mount /dev/btcm/portage /mnt/gentoo/usr/portage
	
	# lvcreate -L20G -nvar btcm
	# mkfs.ext4 /dev/btcm/var
	# mkdir /mnt/gentoo/var
	# mount /dev/btcm/var /mnt/gentoo/var

install gentoo installation files
	# cd /mnt/gentoo
	# wget -nd -nH -r -l 1 -A "stage3-amd64-hardened-*" http://gentoo.mirrors.tds.net/gentoo/releases/amd64/autobuilds/current-stage3-amd64-hardened/
	# gpg --keyserver subkeys.pgp.net --recv-keys 2D182910 17072058
	# gpg --verify stage3-*.tar.bz2.DIGESTS.asc
	# sha1sum -c stage3-*.tar.bz2.DIGESTS.asc
	# tar xvjpf stage3-*.tar.bz2
	edit /etc/make.conf (see ENVIR/etc/make.conf)

install gentoo base system
	# cp -L /etc/resolv.conf /mnt/gentoo/etc/
	# mount -t proc none /mnt/gentoo/proc
	# mount --rbind /dev /mnt/gentoo/dev
	# chroot /mnt/gentoo /bin/bash
	# env-update
	# source /etc/profile
	# emerge --sync
	# eselect profile set `eselect profile list|grep "hardened/linux/amd64\($\| \*\)"|sed "s|.\+\[\([0-9]\+\)\].\+|\1|"`
	# emerge --oneshot binutils gcc virtual/libc
	# emerge --oneshot libtool ghc
	# cat /usr/share/i18n/SUPPORTED >> /etc/locale.gen
	# locale-gen

configure kernel
	# cp /usr/share/zoneinfo/UTC /etc/localtime
	# emerge gentoo-sources
	# cd /usr/src/linux
	edit /usr/src/linux/.config (see ENVIR/usr/src/linux/.config)
	# make oldconfig
	# make -j3 && make modules_install
	# cp arch/x86_64/boot/bzImage /boot/kernel-`file -L arch/x86_64/boot/bzImage|sed "s/.* version \([^ ]*\) .*/\1/"`

configure system
	edit /etc/fstab (see ENVIR/etc/fstab)
	edit /etc/conf.d/hostname (see ENVIR/etc/conf.d/hostname)
	edit /etc/issue (see ENVIR/etc/issue)
	edit /etc/conf.d/net (see ENVIR/etc/conf.d/net)
	# cd /etc/init.d
	# ln -s net.lo net.eth0
	# rc-update add net.eth0 default
	# passwd
	# useradd -m -G users btcm
	# passwd btcm

install system tools
	# emerge app-misc/screen
	create and edit ~/.screen for bitcoinmarkets user (see ENVIR/home/btcm/.screenrc)
	# emerge lvm2
	# rc-update add lvm boot
	# emerge syslog-ng
	# rc-update add syslog-ng default
	# emerge vixie-cron
	# rc-update add vixie-cron default
	# emerge mlocate
	# emerge dhcpcd
	# rc-update add sshd default

configure grub
	# emerge grub
	# grep -v rootfs /proc/mounts > /etc/mtab
	edit /boot/grub/grub.conf (see ENVIR/boot/grub/grub.conf)
	# grub --no-floppy
		root (hd0,0)
		setup (hd0)
		quit

reboot into installed environment
	# exit
	# cd
	# umount -l /mnt/gentoo/boot
	# umount -l /mnt/gentoo/dev
	# umount -l /mnt/gentoo/home
	# umount -l /mnt/gentoo/opt
	# umount -l /mnt/gentoo/proc
	# umount -l /mnt/gentoo/usr/portage
	# umount -l /mnt/gentoo/usr
	# umount -l /mnt/gentoo/var
	# vgchange -a n
	# reboot

update system tools and software environment
	# emerge -uNDv system world

configure web server
	# emerge lighttpd
	edit /etc/lighttpd/lighttpd.conf (see ENVIR/etc/lighttpd/lighttpd.conf)
	# mkdir -p /var/www/bitcoinmarkets.com/{htdocs,logs}
	# /etc/init.d/lighttpd start
	# rc-update add lighttpd default

configure sql server
	# emerge phppgadmin postgresql-server psycopg
	edit /etc/vhosts/webapp-config (see ENVIR/etc/vhosts/webapp-config)
		vhost_server="lighttpd"
	# webapp-config -I -h bitcoinmarkets.com -d phppgadmin phppgadmin 5.0.3
	edit /var/www/bitcoinmarkets.com/htdocs/phppgadmin/conf/config.inc.php (see ENVIR/var/www/bitcoinmarkets.com/htdocs/phppgadmin/conf/config.inc.php)x
		$conf['servers'][0]['host'] = 'localhost';
	# emerge --config postgresql-server
	see http://www.postgresql.org/docs/current/static/runtime-config-resource.html#GUC-SHARED-BUFFERS
	edit /etc/postgresql-9.1/postgresql.conf (see ENVIR/etc/postgresql-9.1/postgresql.conf)
		shared_buffers = 64MB
		max_connections = 200
	edit /etc/postgresql-9.1/pg_hba.conf (see ENVIR/etc/postgresql-9.1/pg_hba.conf)
		temporarily change line to: local all all trust
	see http://www.postgresql.org/docs/current/static/kernel-resources.html
	# sysctl -w kernel.shmmax=268435456
	# sysctl -w kernel.shmall=65536
	edit /etc/sysctl.conf (see ENVIR/etc/sysctl.conf)
	# /etc/init.d/postgresql-9.1 start
	# psql -U postgres
		\password
		enter password twice
		^D
	create super user for access with phppgadmin
		# createuser -lPsU postgres [nameforsuperuser]
			enter password twice
	edit /etc/postgresql-9.1/pg_hba.conf (see ENVIR/etc/postgresql-9.1/pg_hba.conf)
		change all methods to md5
		local   all             all                                     md5
		host    all             all             127.0.0.1/32            md5
		host    all             all             ::1/128                 md5
	# usermod -a -G postgres btcm
	# /etc/init.d/postgresql-9.1 restart
	# rc-update add postgresql-9.1 default
	see README to continue

optionally install additional applications for convenience
	# emerge bc bmon eix hdparm iptables links lynx nmap parted ufed