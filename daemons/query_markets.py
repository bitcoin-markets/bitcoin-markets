#!/usr/bin/python
import json, os, psycopg2, stat, time, tempfile

def writeJSONToFile(json_data, file):
    f = tempfile.NamedTemporaryFile(delete=False, dir='tmp/')
    os.chmod(f.name,stat.S_IREAD | stat.S_IWRITE | stat.S_IRGRP | stat.S_IROTH)
    f.write(json.dumps(json_data))
    f.close()
    os.rename(f.name, file)

if os.path.exists('tmp/') == False: os.mkdir('tmp/')

while True:
    db = psycopg2.connect(database="bitcoinmarkets",user="bitcoinmarkets")
    c = db.cursor()
    c.execute("""SELECT
        market_id,
        market_exchange_id,
        market_symbol,
        market_currency_id,
        market_website,
        market_data_depth,
        market_data_depth_all,
        market_data_ticker,
        market_data_trades,
        market_data_trades_all,
        market_data_trades_canceled,
        market_active,
        market_transactionfee,
        market_type
        FROM markets""")
    markets = []
    for result in c.fetchall():
        (mid,
        mexchange_id,
        msymbol,
        mcurrency_id,
        mwebsite,
        mdata_depth,
        mdata_depth_all,
        mdata_ticker,
        mdata_trades,
        mdata_trades_all,
        mdata_trades_canceled,
        mactive,
        mtransactionfee,
        mtype) = result
        markets.append({
            'mid':mid,
            'mexchange_id':mexchange_id,
            'msymbol':msymbol,
            'mcurrency_id':mcurrency_id,
            'mwebsite':mwebsite,
            'mdata_depth':mdata_depth,
            'mdata_depth_all':mdata_depth_all,
            'mdata_ticker':mdata_ticker,
            'mdata_trades':mdata_trades,
            'mdata_trades_all':mdata_trades_all,
            'mdata_trades_canceled':mdata_trades_canceled,
            'mactive':mactive,
            'mtransactionfee':mtransactionfee,
            'mtype':mtype
        })
    writeJSONToFile(markets, 'data/markets')
    c.close()
    db.close()
    time.sleep(600)