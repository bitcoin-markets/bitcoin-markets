#!/usr/bin/python
import json, os, psycopg2, stat, time, tempfile

def writeJSONToFile(json_data, file):
    f = tempfile.NamedTemporaryFile(delete=False, dir='tmp/')
    os.chmod(f.name,stat.S_IREAD | stat.S_IWRITE | stat.S_IRGRP | stat.S_IROTH)
    f.write(json.dumps(json_data))
    f.close()
    os.rename(f.name, file)

if os.path.exists('tmp/') == False: os.mkdir('tmp/')

while True:
    db = psycopg2.connect(database="bitcoinmarkets",user="bitcoinmarkets")
    c = db.cursor()
    c.execute("""SELECT currency_id, currency_abbreviation, currency_name FROM currencies""")
    currencies = []
    for result in c.fetchall():
        (cid, cabbr, cname) = result
        currencies.append({'cid':cid, 'cabbr':cabbr, 'cname':cname})
    writeJSONToFile(currencies, 'data/currencies')
    c.close()
    db.close()
    time.sleep(600)