#!/usr/bin/python
import json, os, psycopg2, stat, time, tempfile

def writeJSONToFile(json_data, file):
    f = tempfile.NamedTemporaryFile(delete=False, dir='tmp/')
    os.chmod(f.name,stat.S_IREAD | stat.S_IWRITE | stat.S_IRGRP | stat.S_IROTH)
    f.write(json.dumps(json_data))
    f.close()
    os.rename(f.name, file)

if os.path.exists('tmp/') == False: os.mkdir('tmp/')

while True:
    db = psycopg2.connect(database="bitcoinmarkets",user="bitcoinmarkets")
    c = db.cursor()
    c.execute("""SELECT
        exchange_id,
        exchange_name,
        exchange_website,
        exchange_email,
        exchange_freenode,
        exchange_bitcointalk,
        exchange_phone,
        exchange_address,
        exchange_devname
        FROM exchanges""")
    exchanges = []
    for result in c.fetchall():
        (eid, ename, ewebsite, eemail, efreenode, ebitcointalk, ephone, eaddress, edevname) = result
        exchanges.append({
            'eid':eid,
            'ename':ename,
            'ewebsite':ewebsite,
            'eemail':eemail,
            'efreenode':efreenode,
            'ebitcointalk':ebitcointalk,
            'ephone':ephone,
            'eaddress':eaddress,
            'edevname':edevname
        })
    writeJSONToFile(exchanges, 'data/exchanges')
    c.close()
    db.close()
    time.sleep(600)