#!/usr/bin/python
import decimal, httplib, json, logging, os, psycopg2, re, socket, ssl, string, tempfile, threading, time

# Levels: DEBUG, INFO, WARNING, ERROR, CRITICAL
logging.basicConfig(level='WARNING');log = logging.getLogger(":")

headers = {'User-Agent': 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/2008121819 Gentoo Firefox/3.0.4','Connection':'keep-alive'}
pushToIRC = True

def writeJSONToFile(json_data, file):
    if file[0:14] == '/dev/shm/btcm/':
        vfs = os.statvfs("/dev/shm/")
        while vfs.f_bfree * vfs.f_bsize / 1024 < 1024:
            time.sleep(1)
            vfs = os.statvfs("/dev/shm/")
        f = tempfile.NamedTemporaryFile(delete=False, dir='/dev/shm/btcm/tmp/')
    else:
        f = tempfile.NamedTemporaryFile(delete=False, dir='tmp/')
        
    f.write(json.dumps(json_data))
    f.close()
    os.rename(f.name, file)

if pushToIRC == True:
    import random, string
if os.path.exists('tmp/') == False: os.mkdir('tmp/')
if os.path.exists('htdocs/data/') == False: os.mkdir('htdocs/data/')
if os.path.exists('htdocs/data/orderbooks/') == False: os.mkdir('htdocs/data/orderbooks/')

class MarketWorker(threading.Thread):
    def __init__(self, exchange):
        self.connection = None
        self.exchange = exchange
        threading.Thread.__init__(self)

    def run(self):
        url_depth = re.search('(https?)://([^/]*)(.*)', self.exchange[2])
        url_ticker = re.search('(https?)://([^/]*)(.*)', self.exchange[1])
        url_trades = re.search('(https?)://([^/]*)(.*)', self.exchange[3])
        #url_trades_canceled = re.search('(https?)://([^/]*)(.*)', self.exchange[4])
        
        if url_depth is None and url_ticker is None and url_trades is None:
            return
        
        lastQueryDepth = 0
        lastQueryTicker = 0
        lastQueryTrades = 0
        
        while True:
            if url_depth is not None:
                if lastQueryDepth + 30 < time.time():
                    lastQueryDepth = time.time()
                    self.getMarketDepth(self.exchange, url_depth)
            if url_ticker is not None:
                if lastQueryTicker + 30 < time.time():
                    self.getMarketTicker(self.exchange, url_ticker)
                    lastQueryTicker = time.time()
            if url_trades is not None:
                queryTrades = False
                
                supportsLastTrade = string.find(url_trades.group(), "%LASTTRADEID%") != -1 or string.find(url_trades.group(), "%LASTTRADETS%") != -1
                if supportsLastTrade and lastQueryTrades + 1 < time.time():
                    queryTrades = True
                
                if lastQueryTrades + 30 < time.time():
                    queryTrades = True
                
                if queryTrades:
                    self.getMarketTrades(self.exchange, url_trades)
                    lastQueryTrades = time.time()
            time.sleep(0.05)

    def addMarketDepthData(self, result, url_depth, orderdepth):
        for ask in orderdepth['asks']:
            if 'low' not in orderdepth['ticker']: orderdepth['ticker']['low'] = float(ask[0])
            elif float(ask[0]) < orderdepth['ticker']['low']: orderdepth['ticker']['low'] = float(ask[0])
        for bid in orderdepth['bids']:
            if 'high' not in orderdepth['ticker']: orderdepth['ticker']['high'] = float(bid[0])
            elif float(bid[0]) > orderdepth['ticker']['high']: orderdepth['ticker']['high'] = float(bid[0])
        if 'high' not in orderdepth['ticker']: orderdepth['ticker']['high'] = None
        if 'low' not in orderdepth['ticker']: orderdepth['ticker']['low'] = None
        if orderdepth['ticker']['high'] is None or orderdepth['ticker']['low'] is None:
            orderdepth['ticker']['avg'] = None
        else:
            orderdepth['ticker']['avg'] = str((decimal.Decimal(orderdepth['ticker']['high']) + decimal.Decimal(orderdepth['ticker']['low'])) / 2)
        if orderdepth['ticker']['high'] is not None and orderdepth['ticker']['low'] is not None:
            writeJSONToFile(orderdepth, 'htdocs/data/orderbooks/'+str(result[0]));
            log.info(str(int(time.time()))+":DEPTH:"+url_depth.group(2)+":"+result[5]+" high:"+str(orderdepth['ticker']['high'])+" low:"+str(orderdepth['ticker']['low']))
            db = psycopg2.connect(database="bitcoinmarkets",user="bitcoinmarkets")
            c = db.cursor()
            c.execute("""INSERT INTO depth (
                    depth_market_id, depth_highbid, depth_lowask
                ) VALUES(%s,%s,%s)""", (
                    result[0], orderdepth['ticker']['high'], orderdepth['ticker']['low']
                )
            )
            db.commit()
            c.close()
            db.close()

    def addMarketTickerData(self, result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap):
        if isinstance(ticker_high, bool): ticker_high = None
        if isinstance(ticker_low, bool): ticker_low = None
        if isinstance(ticker_avg, bool): ticker_avg = None
        if isinstance(ticker_vwap, bool): ticker_vwap = None
        if isinstance(ticker_vol, bool): ticker_vol = None
        if isinstance(ticker_last, bool): ticker_last = None
        if isinstance(ticker_buy, bool): ticker_buy = None
        if isinstance(ticker_sell, bool): ticker_sell = None
        log.info(str(int(time.time()))+":TICKER:"+url_ticker.group(2)+":"+result[5]+" high:"+str(ticker_high)+" low:"+str(ticker_low)+" avg:"+str(ticker_avg)+" vwap:"+str(ticker_vwap)+" vol:"+str(ticker_vol)+" last:"+str(ticker_last)+" buy:"+str(ticker_buy)+" sell:"+str(ticker_sell))
        db = psycopg2.connect(database="bitcoinmarkets",user="bitcoinmarkets")
        c = db.cursor()
        c.execute("""INSERT INTO tickers (
                ticker_market_id, ticker_high, ticker_low, ticker_avg, ticker_vwap, ticker_vol, ticker_last, ticker_buy, ticker_sell
            ) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s)""", (
                result[0], ticker_high, ticker_low, ticker_avg, ticker_vwap, ticker_vol, ticker_last, ticker_buy, ticker_sell
            )
        )
        db.commit()
        c.close()

    def addMarketTradesData(self, result, url_trades, tradehistory):
        tradehistory['trades'].sort(key=lambda x: x['date'])
        db = psycopg2.connect(database="bitcoinmarkets",user="bitcoinmarkets")
        c = db.cursor()
        for trade in tradehistory['trades']:
            if 'primary' not in trade: trade['primary'] = True
            c.execute("""SELECT trade_market_id, trade_id FROM trades WHERE trade_market_id=%s AND trade_id=%s""", (result[0], trade['tradeid']))
            if c.fetchone() is None:
                log.info(str(int(time.time()))+":TRADE:"+url_trades.group(2)+":"+result[5]+" tid:"+str(trade['tradeid'])+" date:"+str(trade['date'])+" price:"+str(trade['price'])+" quantity:"+str(trade['quantity']))
                c.execute("""INSERT INTO trades (
                        trade_market_id, trade_id, trade_date, trade_price, trade_quantity, trade_primary
                    ) VALUES(%s,%s,to_timestamp(%s),%s,%s,%s)""", (
                        result[0], trade['tradeid'], trade['date'], trade['price'], trade['quantity'], trade['primary']
                    )
                )
                db.commit()
                if pushToIRC == True and trade['primary'] == True:
                    trade['currency'] = result[5]
                    trade['market'] = result[6]
                    trade['price'] = str(trade['price'])
                    trade['quantity'] = str(trade['quantity'])
                    trade['date'] = str(trade['date'])
                    writeJSONToFile(trade, '/dev/shm/btcm/irc/trades/'+str(trade['date'])+'_'+''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32)))
        c.close()
        db.close()

    def getJSON(self, url):
        if self.connection is None:
            if url.group(1) == "http": self.connection = httplib.HTTPConnection(url.group(2), timeout=10)
            elif url.group(1) == "https": self.connection = httplib.HTTPSConnection(url.group(2), timeout=10)

        try:
            self.connection.request("GET", url.group(3), '', headers)
        except httplib.BadStatusLine, e:
            log.error("http status code error:"+url.group()+" "+e.message)
            self.connection.close()
            return
        except httplib.CannotSendRequest, e:
            self.connection.close()
            self.connection.connect()
            self.connection.request("GET", url.group(3), '', headers)
        except socket.error, e:
            log.error("socket error:"+url.group()+" "+str(e))
            self.connection.close()
            return
        except socket.timeout, e:
            log.error("socket timeout:"+url.group()+" "+e.message)
            self.connection.close()
            return

        try:
            response = self.connection.getresponse()
        except httplib.ResponseNotReady, e:
            log.error("http response not ready:"+url.group()+" "+e.message)
            self.connection.close()
            return
        except socket.error, e:
            log.error("socket error:"+url.group()+" "+e.message)
            self.connection.close()
            return
        except ssl.SSLError, e:
            log.error("ssl error:"+url.group()+" "+e.message)
            self.connection.close()
            return

        try:
            return json.load(response, parse_float=decimal.Decimal)
        except Exception, e:
            log.error("exception:"+url.group()+" "+e.message+" "+str(response))
            self.connection.close()
            return

    def initMarketDepthVars(self, url_depth):
        json_depth = self.getJSON(url_depth)
        if json_depth is None or json_depth == {}: return (None, None)
        orderdepth = {u'asks':[],u'bids':[],u'ticker':{}}
        return (json_depth, orderdepth)

    def initMarketTickerVars(self, url_ticker):
        json_ticker = self.getJSON(url_ticker)
        if json_ticker is None or json_ticker == {}: return
        return json_ticker

    def initMarketTradesVars(self, result, url_trades):
        if string.find(url_trades.group(), "%LASTTRADEID%") != -1:
            db = psycopg2.connect(database="bitcoinmarkets",user="bitcoinmarkets")
            c = db.cursor()
            c.execute("""SELECT trade_id FROM trades WHERE trade_market_id = %s ORDER BY trade_date DESC LIMIT 1""", (result[0],))
            r = c.fetchone()
            c.close()
            db.close()
            if r is None: tradeid = 0
            else: tradeid = r[0]
            url_trades = string.replace(url_trades.group(), "%LASTTRADEID%", str(tradeid))
            url_trades = re.search('(https?)://([^/]*)(.*)', url_trades)
        elif string.find(url_trades.group(), "%LASTTRADETS%") != -1:
            db = psycopg2.connect(database="bitcoinmarkets",user="bitcoinmarkets")
            c = db.cursor()
            c.execute("""SELECT EXTRACT(EPOCH FROM trade_date) FROM trades WHERE trade_market_id = %s ORDER BY trade_date DESC LIMIT 1""", (result[0],))
            r = c.fetchone()
            c.close()
            db.close()
            if r is None: tradets = 0
            else: tradets = r[0]
            url_trades = string.replace(url_trades.group(), "%LASTTRADETS%", str(int(tradets)))
            url_trades = re.search('(https?)://([^/]*)(.*)', url_trades)
        json_trades = self.getJSON(url_trades)
        if json_trades is None or json_trades == {}: return (None, None)
        tradehistory = {u'trades':[]}
        return (json_trades, tradehistory)

class aqoin(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(str(bid[1]))])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class bitchange(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(str(bid[1]))])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = json_ticker['high']
        ticker_low = json_ticker['low']
        ticker_avg = json_ticker['avg']
        ticker_vwap = json_ticker['vwap']
        ticker_vol = json_ticker['vol']
        ticker_last = json_ticker['last']
        ticker_buy = json_ticker['buy']
        ticker_sell = json_ticker['sell']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class bitcoin2cash(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        #self.addMarketTradesData(result, url_trades, tradehistory)

class bitcoin7(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        if 'asks' in json_depth:
            for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        if 'bids' in json_depth:
            for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = json_ticker['ticker']['high']
        ticker_low = json_ticker['ticker']['low']
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker['ticker']['vol']
        ticker_last = json_ticker['ticker']['last']
        ticker_buy = json_ticker['ticker']['buy']
        ticker_sell = json_ticker['ticker']['sell']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        log.warning("Awaiting understandable trade history json data "+url_trades.group())
        #self.addMarketTradesData(result, url_trades, tradehistory)

class bitcoin_de(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        #self.addMarketTradesData(result, url_trades, tradehistory)

class bitcoincentral(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask['price']),str(ask['volume'])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid['price']),str(bid['volume'])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = json_ticker['high']
        ticker_low = json_ticker['low']
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker['volume']
        ticker_last = json_ticker['last_trade']['price']
        ticker_buy = json_ticker['buy']
        ticker_sell = json_ticker['sell']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        log.warning("Awaiting tradeid to be included in JSON data "+url_trades.group())
        for trade in json_trades:
            trade_currency = trade['currency'].replace('PGAU', 'PXGAU')
            #if trade_currency == result[5]:
                # Awaiting tradeid to be included in JSON data
                #tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tradeid'],"price":trade['price'],"quantity":trade['amount']})
        #self.addMarketTradesData(result, url_trades, tradehistory)

class bitcoinica(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        #for ask in json_depth['asks']: orderdepth['asks'].append([str(ask['price']),str(ask['volume'])])
        #for bid in json_depth['bids']: orderdepth['bids'].append([str(bid['price']),str(bid['volume'])])
        #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #ticker_high = json_ticker['highest']
        #ticker_low = json_ticker['lowest']
        #ticker_avg = None
        #ticker_vwap = None
        #ticker_vol = None
        #ticker_last = None
        #icker_buy = None
        #ticker_sell = None
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        #for trade in json_trades:
        #    if trade['pair'] == 'BTC'+result[5]:
        #        trade_ts = time.mktime(datetime.datetime.strptime(trade['created_at'], '%Y-%m-%dT%H:%M:%SZ').timetuple())
        #        tradehistory['trades'].append({"date":trade_ts,"tradeid":trade['tradeid'],"price":trade['price'],"quantity":trade['amount']})
        #self.addMarketTradesData(result, url_trades, tradehistory)

class bitcoinmarket(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = None
        ticker_low = None
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker['BMBTC/'+result[5]]['volume']
        ticker_last = json_ticker['BMBTC/'+result[5]]['last_price']
        ticker_buy = None
        ticker_sell = None
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades:
            if trade['last_action'] == 'Finalized':
                tradehistory['trades'].append({"date":trade['last_action_datetime'],"tradeid":None,"price":trade['price'],"quantity":trade['amount']})
        #self.addMarketTradesData(result, url_trades, tradehistory)

class bitcoiny(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        #self.addMarketTradesData(result, url_trades, tradehistory)

class bitfloor(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        pass
        #json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        #if json_depth is None: return
        #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        pass
        #json_ticker = self.initMarketTickerVars(url_ticker)
        #if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        pass
        #json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        #if json_trades is None: return
        #self.addMarketTradesData(result, url_trades, tradehistory)

class bitmarket(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = None
        ticker_low = None
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = None
        ticker_last = json_ticker['currencies'][result[5]]['last']
        ticker_buy = json_ticker['currencies'][result[5]]['buy']
        ticker_sell = json_ticker['currencies'][result[5]]['sell']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tx_id'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class bitnz(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class bitparking(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = json_ticker['ticker']['high']
        ticker_low = json_ticker['ticker']['low']
        ticker_avg = json_ticker['ticker']['average']
        ticker_vwap = None
        ticker_vol = json_ticker['ticker']['vol']
        ticker_last = json_ticker['ticker']['last']
        ticker_buy = None
        ticker_sell = None
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades:
            price = 1 / float(trade['price'])
            quantity = float(trade['amount']) / price
            tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['id'],"price":price,"quantity":quantity})
        self.addMarketTradesData(result, url_trades, tradehistory)

class bitstamp(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        #self.addMarketTradesData(result, url_trades, tradehistory)

class brasilbitcoinmarket(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class britcoin(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = None
        ticker_low = None
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker['ticker']['vol']
        ticker_last = json_ticker['ticker']['last']
        ticker_buy = json_ticker['ticker']['buy']
        ticker_sell = json_ticker['ticker']['sell']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class btce(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = json_ticker['ticker']['high']
        ticker_low = json_ticker['ticker']['low']
        ticker_avg = json_ticker['ticker']['avg']
        ticker_vwap = None
        ticker_vol = json_ticker['ticker']['vol']
        ticker_last = json_ticker['ticker']['last']
        ticker_buy = json_ticker['ticker']['buy']
        ticker_sell = json_ticker['ticker']['sell']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class btcex(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = None
        ticker_low = None
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker[0]['last24HrsTradedQuantity']
        ticker_last = json_ticker[0]['lastTradedPrice']
        ticker_buy = json_ticker[0]['bid']
        ticker_sell = json_ticker[0]['ask']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class campbx(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['Asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['Bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = None
        ticker_low = None
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = None
        ticker_last = json_ticker['Last Trade']
        ticker_buy = json_ticker['Best Bid']
        ticker_sell = json_ticker['Best Ask']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['Time'],"tradeid":trade['Order ID'],"price":trade['Price'],"quantity":trade['Bitcoins']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class coinabul(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        #self.addMarketTradesData(result, url_trades, tradehistory)

class copbe(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = json_ticker['ticker']['high']
        ticker_low = json_ticker['ticker']['low']
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker['ticker']['vol']
        ticker_last = json_ticker['ticker']['last']
        ticker_buy = json_ticker['ticker']['buy']
        ticker_sell = json_ticker['ticker']['sell']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class exchb(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = json_ticker['ticker']['high']
        ticker_low = json_ticker['ticker']['low']
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker['ticker']['vol']
        ticker_last = json_ticker['ticker']['last']
        ticker_buy = json_ticker['ticker']['buy']
        ticker_sell = json_ticker['ticker']['sell']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class freshbtc(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        #self.addMarketTradesData(result, url_trades, tradehistory)

class globalbitcoinexchange(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        #self.addMarketTradesData(result, url_trades, tradehistory)

class hellobitcoin(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for order in json_depth:
            if order['side'] == 'ask': orderdepth['asks'].append([str(order['price']), str(order['quantity'])])
            elif order['side'] == 'bid': orderdepth['bids'].append([str(order['price']), str(order['quantity'])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = None
        ticker_low = None
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker['volume']
        ticker_last = json_ticker['last']
        ticker_buy = json_ticker['bid']
        ticker_sell = json_ticker['ask']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['timestamp_posix'],"tradeid":None,"price":trade['price'],"quantity":trade['quantity']})
        #self.addMarketTradesData(result, url_trades, tradehistory)

class intersango(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = None
        ticker_low = None
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker['vol']
        ticker_last = json_ticker['last']
        ticker_buy = json_ticker['buy']
        ticker_sell = json_ticker['sell']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class mercadobitcoin(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = json_ticker['ticker']['high']
        ticker_low = json_ticker['ticker']['low']
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker['ticker']['vol']
        ticker_last = json_ticker['ticker']['last']
        ticker_buy = json_ticker['ticker']['buy']
        ticker_sell = json_ticker['ticker']['sell']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class mtgox(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = json_ticker['ticker']['high']
        ticker_low = json_ticker['ticker']['low']
        ticker_avg = json_ticker['ticker']['avg']
        ticker_vwap = json_ticker['ticker']['vwap']
        ticker_vol = json_ticker['ticker']['vol']
        ticker_last = json_ticker['ticker']['last']
        ticker_buy = json_ticker['ticker']['buy']
        ticker_sell = json_ticker['ticker']['sell']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        #print json_trades
        if 'return' in json_trades: # API version 1
            for trade in json_trades['return']:
                if trade['primary'] == 'Y': trade['primary'] = True
                else: trade['primary'] = False
                tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount'],"primary":trade['primary']})
        else: # API version 0
            for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)
    def getMarketTradesCanceled(self, result, url_trades_canceled):
        return

class ozbitcoin(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        #self.addMarketTradesData(result, url_trades, tradehistory)

class ruxum(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        if url_depth.group(0)[0:28] == 'https://x.ruxum.com/api/btc':
            for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
            for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
            self.addMarketDepthData(result, url_depth, orderdepth) 
        else:
            # Convert values from btc/??? to ???/btc?  Also fix for bitparking?
            pass
            #for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
            #for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
            #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        if url_ticker.group(0)[0:28] == 'https://x.ruxum.com/api/btc':
            ticker_high = json_ticker['high']
            ticker_low = json_ticker['low']
            ticker_avg = None
            ticker_vwap = None
            ticker_vol = json_ticker['volume']
            ticker_last = json_ticker['last_trade']['price']
            ticker_buy = json_ticker['bid']
            ticker_sell = json_ticker['ask']
            self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
        else:
            # Fix for nmc/btc?
            pass
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        if url_trades.group(0)[0:28] == 'https://x.ruxum.com/api/btc':
            for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
            self.addMarketTradesData(result, url_trades, tradehistory)
        else:
            # Fix for nmc/btc?
            pass

class solidcoin24(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask['rate']),str(ask['amount'])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid['rate']),str(bid['amount'])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = json_ticker['high']
        ticker_low = json_ticker['low']
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker['vol']
        ticker_last = json_ticker['last']
        ticker_buy = json_ticker['bid']
        ticker_sell = json_ticker['ask']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades:
            price = 1 / float(trade['rate'])
            quantity = float(trade['amount']) / price
            tradehistory['trades'].append({"date":trade['time'],"tradeid":trade['id'],"price":price,"quantity":quantity})
        self.addMarketTradesData(result, url_trades, tradehistory)

class therocktradingco(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class tradebtc(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        #self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        #self.addMarketTradesData(result, url_trades, tradehistory)

class tradehill(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = string.replace(json_ticker['ticker']['high'], "None", "0")
        ticker_low = string.replace(json_ticker['ticker']['low'], "None", "0")
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = string.replace(json_ticker['ticker']['vol'], "None", "0")
        ticker_last = string.replace(string.replace(json_ticker['ticker']['last'], "None", "0"), "", "0")
        ticker_buy = string.replace(json_ticker['ticker']['buy'], "None", "0")
        ticker_sell = string.replace(json_ticker['ticker']['sell'], "None", "0")
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class virwox(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['result'][0]['sell']: orderdepth['asks'].append([str(ask['price']),str(ask['volume'])])
        for bid in json_depth['result'][0]['buy']: orderdepth['bids'].append([str(bid['price']),str(bid['volume'])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        #missing ticker?
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades['result']['data']: tradehistory['trades'].append({"date":trade['time'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['vol']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class virtex(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = json_ticker['high']
        ticker_low = json_ticker['low']
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker['volume']
        ticker_last = json_ticker['last']
        ticker_buy = None
        ticker_sell = None
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

class worldbitcoinexchange(MarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask[0]),str(ask[1])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid[0]),str(bid[1])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = None
        ticker_low = None
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker['ticker']['vol']
        ticker_last = json_ticker['ticker']['last']
        ticker_buy = json_ticker['ticker']['buy']
        ticker_sell = json_ticker['ticker']['sell']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['date'],"tradeid":trade['tid'],"price":trade['price'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

marketworkers = []
db = psycopg2.connect(database="bitcoinmarkets",user="bitcoinmarkets")
c = db.cursor()
c.execute("""SELECT
        market_id,
        market_data_ticker,
        market_data_depth,
        market_data_trades,
        market_data_trades_canceled,
        currency_abbreviation,
        exchange_devname
    FROM markets,exchanges,currencies
    WHERE markets.market_currency_id=currency_id
    AND markets.market_exchange_id=exchanges.exchange_id
    AND markets.market_active=TRUE
    AND (
        markets.market_data_depth != '' or
        markets.market_data_ticker != '' or
        markets.market_data_trades != ''
    )
    """)

threading.stack_size(131072)

for result in c.fetchall():
    w = {
        'aqoin':aqoin,
        'bitchange':bitchange,
        'bitcoin2cash':bitcoin2cash,
        'bitcoin7':bitcoin7,
        'bitcoin_de':bitcoin_de,
        'btcncentral':bitcoincentral,
        'bitcoinica':bitcoinica,
        'btcnmarket':bitcoinmarket,
        'bitcoiny':bitcoiny,
        'bitfloor':bitfloor,
        'bitmarket':bitmarket,
        'bitnz':bitnz,
        'bitparking':bitparking,
        'bitstamp':bitstamp,
        'brslbtcnmrkt':brasilbitcoinmarket,
        'britcoin':britcoin,
        'btce':btce,
        'btcex':btcex,
        'campbx':campbx,
        'coinabul':coinabul,
        'copbe':copbe,
        'exchb':exchb,
        'freshbtc':freshbtc,
        'glblbitcoin':globalbitcoinexchange,
        'hellobitcoin':hellobitcoin,
        'intersango':intersango,
        'mercadobtcn':mercadobitcoin,
        'mtgox':mtgox,
        'ozbitcoin':ozbitcoin,
        'ruxum':ruxum,
        'solidcoin24':solidcoin24,
        'rocktrading':therocktradingco,
        'tradebtc':tradebtc,
        'tradehill':tradehill,
        'virtex':virtex,
        'virwox':virwox,
        'worldbitcoin':worldbitcoinexchange
    }[result[6]](result)
    w.daemon = True
    w.start()
    marketworkers.append(w)
c.close()
db.close()

#raw_input()
while True:
    time.sleep(1)
