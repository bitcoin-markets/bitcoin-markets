#!/usr/bin/python
import json, logging, os, psycopg2, time, tempfile

# Levels: DEBUG, INFO, WARNING, ERROR, CRITICAL
logging.basicConfig(level='WARNING');log = logging.getLogger(":")

def writeJSONToFile(json_data, file):
    f = tempfile.NamedTemporaryFile(delete=False, dir='tmp/')
    f.write(json.dumps(json_data))
    f.close()
    os.rename(f.name, 'htdocs/data/'+file)

if os.path.exists('tmp/') == False: os.mkdir('tmp/')
if os.path.exists('htdocs/data/') == False: os.mkdir('htdocs/data/')
if os.path.exists('htdocs/data/markets/') == False: os.mkdir('htdocs/data/markets/')

while True:
    db = psycopg2.connect(database="bitcoinmarkets",user="bitcoinmarkets")
    c = db.cursor()
    c.execute("""SELECT market_id, market_symbol, market_website, market_currency_id,
        currency_abbreviation, currency_name,
        (trade_latest).trade_id AS trade_latest_trade_id,
        (trade_latest).trade_date AS trade_latest_trade_date,
        (trade_latest).trade_price AS trade_latest_trade_price,
        (trade_latest).trade_quantity AS trade_latest_trade_quantity,
        (trade_previousclose).trade_id AS trade_previousclose_trade_id,
        (trade_previousclose).trade_date AS trade_previousclose_trade_date,
        (trade_previousclose).trade_price AS trade_previousclose_trade_price,
        (trade_previousclose).trade_quantity AS trade_previousclose_trade_quantity,
        (trade_open).trade_id AS trade_open_trade_id,
        (trade_open).trade_date AS trade_open_trade_date,
        (trade_open).trade_price AS trade_open_trade_price,
        (trade_open).trade_quantity AS trade_open_trade_quantity,
        (trade_ticker).depth_highbid AS trade_highbid,
        (trade_ticker).depth_lowask AS trade_lowask,
        (trade_dayhigh).trade_price AS trade_dayhigh,
        (trade_daylow).trade_price AS trade_daylow
        FROM (
        	SELECT m.market_id AS market_id,
        		m.market_symbol AS market_symbol,
        		m.market_website AS market_website,
        		m.market_currency_id AS market_currency_id,
        		c.currency_abbreviation AS currency_abbreviation,
        		c.currency_name AS currency_name,
        	(
        		SELECT t FROM trades t
        		WHERE t.trade_market_id=m.market_id
        		ORDER BY trade_date DESC LIMIT 1
        	) as trade_latest,
        	(
        		SELECT t FROM trades t
        		WHERE t.trade_market_id=m.market_id AND t.trade_date < date_trunc('day', now() at time zone 'UTC') at time zone 'UTC'
        		ORDER BY trade_date DESC LIMIT 1
        	) as trade_previousclose,
        	(
        		SELECT t FROM trades t WHERE t.trade_market_id=m.market_id AND t.trade_date > (
        			SELECT t.trade_date FROM trades t
        			WHERE t.trade_market_id=m.market_id AND t.trade_date < date_trunc('day', now() at time zone 'UTC') at time zone 'UTC'
        			ORDER BY t.trade_date DESC LIMIT 1
        		) ORDER BY t.trade_date ASC LIMIT 1
        	) as trade_open,
        	(
        		SELECT SUM(t.trade_quantity) FROM trades t
        		WHERE t.trade_market_id=m.market_id AND t.trade_date >= (now() - interval '24 hours')
        	) as trade_dailyvolumequantity,
        	(
        		SELECT ROUND(SUM(t.trade_quantity * t.trade_price), 2) FROM trades t
        		WHERE t.trade_market_id=m.market_id AND t.trade_date >= (now() - interval '24 hours')
        	) as trade_dailyvolumeprice,
        	(
        		SELECT
        			(SELECT SUM(tv.trade_volume_daily_volume) FROM trade_volumes tv
        			WHERE tv.trade_volume_market_id=m.market_id
        			AND tv.trade_volume_day >= date_trunc('day', (now() - interval '30 days') at time zone 'UTC') at time zone 'UTC'
        			AND tv.trade_volume_day < date_trunc('day', (now() - interval '1 day') at time zone 'UTC') at time zone 'UTC')
        			+
        			(SELECT SUM(t.trade_quantity) FROM trades t
        			WHERE t.trade_market_id=m.market_id
        			AND (
        				(
        					t.trade_date >= (now() - interval '30 days')
        					AND t.trade_date < date_trunc('day', (now() - interval '29 days') at time zone 'UTC') at time zone 'UTC'
        				)
        				OR
        				t.trade_date >= date_trunc('day', (now() - interval '1 day') at time zone 'UTC') at time zone 'UTC'
        			))
        	) as trade_monthlyvolumequantity,
        	(
        		SELECT
        			(SELECT SUM(tv.trade_volume_daily_spent) FROM trade_volumes tv
        			WHERE tv.trade_volume_market_id=m.market_id
        			AND tv.trade_volume_day >= date_trunc('day', (now() - interval '30 days') at time zone 'UTC') at time zone 'UTC'
        			AND tv.trade_volume_day < date_trunc('day', (now() - interval '1 day') at time zone 'UTC') at time zone 'UTC')
        			+
        			(SELECT SUM(t.trade_quantity * t.trade_price) FROM trades t
        			WHERE t.trade_market_id=m.market_id
        			AND (
        				(
        					t.trade_date >= (now() - interval '30 days')
        					AND t.trade_date < date_trunc('day', (now() - interval '29 days') at time zone 'UTC') at time zone 'UTC'
        				)
        				OR
        				t.trade_date >= date_trunc('day', (now() - interval '1 day') at time zone 'UTC') at time zone 'UTC'
        			))
        	) as trade_monthlyvolumeprice,
        	(
        		SELECT d FROM depth d WHERE d.depth_market_id=m.market_id ORDER BY depth_timestamp DESC LIMIT 1
        	) as trade_ticker,
        	(
        		SELECT t FROM trades t
        		WHERE t.trade_market_id=m.market_id AND t.trade_date >= date_trunc('day', now() at time zone 'UTC') at time zone 'UTC'
        		ORDER BY t.trade_price ASC LIMIT 1
        	) as trade_daylow,
        	(
        		SELECT t FROM trades t
        		WHERE t.trade_market_id=m.market_id AND t.trade_date >= date_trunc('day', now() at time zone 'UTC') at time zone 'UTC'
        		ORDER BY t.trade_price DESC LIMIT 1
        	) as trade_dayhigh
        	FROM markets m, currencies c
        	WHERE market_currency_id=currency_id
        ) s ORDER BY trade_monthlyvolumequantity DESC NULLS LAST""")

#c.arraysize = 4096
#while True:
#    results = c.fetchmany()
#    if len(results) == 0: break
#    for result in results: print result

    results = c.fetchall()
    for result in results:
        (market_id, 
        market_symbol,
        market_website,
        currency_id,
        currency_abbreviation,
        currency_name,
        trade_latest_trade_id,
        trade_latest_trade_date,
        trade_latest_trade_price,
        trade_latest_trade_quantity,
        trade_previousclose_trade_id,
        trade_previousclose_trade_date,
        trade_previousclose_trade_price,
        trade_previousclose_trade_quantity,
        trade_open_trade_id,
        trade_open_trade_date,
        trade_open_trade_price,
        trade_open_trade_quantity,
        trade_highbid,
        trade_lowask,
        trade_dayhigh,
        trade_daylow) = result

        if os.path.exists('htdocs/data/markets/'+str(market_id)) == False:
            os.mkdir('htdocs/data/markets/'+str(market_id))

        json_data = {
            'market_symbol':market_symbol,
            'market_website':market_website,
            'currency_abbr':currency_abbreviation,
            'currency_name':currency_name
        }
        if trade_latest_trade_id is not None:
            json_data['latest_trade_id'] = trade_latest_trade_id,
            json_data['latest_trade_date'] = time.mktime(trade_latest_trade_date.timetuple()),
            json_data['latest_trade_price'] = str(trade_latest_trade_price),
            json_data['latest_trade_quantity'] = str(trade_latest_trade_quantity),
        if trade_previousclose_trade_id is not None:
            json_data['previousclose_trade_id'] = trade_previousclose_trade_id,
            json_data['previousclose_trade_date'] = time.mktime(trade_previousclose_trade_date.timetuple()),
            json_data['previousclose_trade_price'] = str(trade_previousclose_trade_price),
            json_data['previousclose_trade_quantity'] = str(trade_previousclose_trade_quantity),
        if trade_open_trade_id is not None:
            json_data['open_trade_id'] = trade_open_trade_id,
            json_data['open_trade_date'] = time.mktime(trade_open_trade_date.timetuple()),
            json_data['open_trade_price'] = str(trade_open_trade_price),
            json_data['open_trade_quantity'] = str(trade_open_trade_quantity),
        if trade_highbid is not None:
            json_data['highbid'] = str(trade_highbid),
        if trade_lowask is not None:
            json_data['lowask'] = str(trade_lowask),
        if trade_dayhigh is not None:
            json_data['dayhigh'] = str(trade_dayhigh),
        if trade_daylow is not None:
            json_data['daylow'] = str(trade_daylow)

        writeJSONToFile(json_data, 'markets/'+str(market_id)+'/all.json')

    c.close()
    db.close()
    time.sleep(1)