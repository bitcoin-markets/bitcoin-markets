#!/usr/bin/python
import json, os, random, stat, string, time, tempfile

def writeJSONToFile(json_data, file):
    f = tempfile.NamedTemporaryFile(delete=False, dir='/dev/shm/btcm/tmp/')
    os.chmod(f.name,stat.S_IREAD | stat.S_IWRITE | stat.S_IRGRP | stat.S_IROTH)
    f.write(json.dumps(json_data))
    f.close()
    os.rename(f.name, file)

currencies = {}; exchanges = {}; last_ticker = {}; markets = {}; tickers = {}
while True:
    try: json_data_currencies = json.load(open('data/currencies'))
    except IOError: continue
    for json_data_currency in json_data_currencies: currencies[json_data_currency['cid']] = json_data_currency

    try: json_data_exchanges = json.load(open('data/exchanges'))
    except IOError: continue
    for json_data_exchange in json_data_exchanges: exchanges[json_data_exchange['eid']] = json_data_exchange

    try: json_data_markets = json.load(open('data/markets'))
    except IOError: continue
    for json_data_market in json_data_markets: markets[json_data_market['mid']] = json_data_market

    try: json_data_trades = json.load(open('data/trades'))
    except IOError: continue
    for json_data_trade in json_data_trades:
        if json_data_trade['mid'] not in tickers:
            tickers[json_data_trade['mid']] = {'avg':'','buy':'','currency':'','dayhigh':'','daylow':'','exchange':'','last':'','market':'','sell':'','vol':'','vwap':''}
        last_ticker['avg'] = tickers[json_data_trade['mid']]['avg']
        last_ticker['buy'] = tickers[json_data_trade['mid']]['buy']
        last_ticker['currency'] = tickers[json_data_trade['mid']]['currency']
        last_ticker['dayhigh'] = tickers[json_data_trade['mid']]['dayhigh']
        last_ticker['daylow'] = tickers[json_data_trade['mid']]['daylow']
        last_ticker['exchange'] = tickers[json_data_trade['mid']]['exchange']
        last_ticker['last'] = tickers[json_data_trade['mid']]['last']
        last_ticker['market'] = tickers[json_data_trade['mid']]['market']
        last_ticker['sell'] = tickers[json_data_trade['mid']]['sell']
        last_ticker['vol'] = tickers[json_data_trade['mid']]['vol']
        last_ticker['vwap'] = tickers[json_data_trade['mid']]['vwap']

        tickers[json_data_trade['mid']]['currency'] = string.lower(currencies[markets[json_data_trade['mid']]['mcurrency_id']]['cabbr'])
        tickers[json_data_trade['mid']]['exchange'] = string.lower(exchanges[markets[json_data_trade['mid']]['mexchange_id']]['edevname'])
        tickers[json_data_trade['mid']]['market'] = markets[json_data_trade['mid']]['msymbol']

        if 'dailyvolumequantity' in json_data_trade:
            if json_data_trade['dailyvolumeprice'] == '':
                tickers[json_data_trade['mid']]['vwap'] = ''
            else:
                tickers[json_data_trade['mid']]['vwap'] = float(json_data_trade['dailyvolumeprice']) / float(json_data_trade['dailyvolumequantity'])
            tickers[json_data_trade['mid']]['vol'] = json_data_trade['dailyvolumequantity']
        if 'highbid' in json_data_trade:
            tickers[json_data_trade['mid']]['buy'] = json_data_trade['highbid']
        if 'lowask' in json_data_trade:
            tickers[json_data_trade['mid']]['sell'] = json_data_trade['lowask']
        if 'dayhigh' in json_data_trade:
            tickers[json_data_trade['mid']]['dayhigh'] = json_data_trade['dayhigh']
        if 'daylow' in json_data_trade:
            tickers[json_data_trade['mid']]['daylow'] = json_data_trade['daylow']
        if 'dailyvolumepriceavg' in json_data_trade:
            tickers[json_data_trade['mid']]['avg'] = json_data_trade['dailyvolumepriceavg']
        if 'latest_trade_price' in json_data_trade:
            tickers[json_data_trade['mid']]['last'] = json_data_trade['latest_trade_price']

        showticker = True
        if last_ticker == {'avg':'','buy':'','currency':'','dayhigh':'','daylow':'','exchange':'','last':'','market':'','sell':'','vol':'','vwap':''}: showticker = False
        if last_ticker == tickers[json_data_trade['mid']]: showticker = False
        if float(tickers[json_data_trade['mid']]['vol']) == 0: showticker = False
        if showticker == True: writeJSONToFile(tickers[json_data_trade['mid']], '/dev/shm/btcm/irc/tickers/'+''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32)))
    time.sleep(0.5)
