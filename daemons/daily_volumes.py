#!/usr/bin/python
import datetime, psycopg2, time

db = psycopg2.connect(database="bitcoinmarkets",user="bitcoinmarkets")
c = db.cursor()

c.execute("""SELECT market_id FROM markets WHERE market_active = true AND market_data_trades != '' ORDER BY market_id""")
markets = [r[0] for r in c.fetchall()]

def getDailyVolume(market, firsttrade, daybeforelasttrade):
    daystart = firsttrade - datetime.timedelta(hours=firsttrade.hour, minutes=firsttrade.minute, seconds=firsttrade.second, microseconds=firsttrade.microsecond)
    while daystart < daybeforelasttrade:
        dayend = daystart + datetime.timedelta(days=1) - datetime.timedelta(microseconds=1)
        c.execute("""SELECT SUM(trade_quantity), SUM(trade_quantity * trade_price) FROM trades
            WHERE trade_market_id = %s
            AND trade_date >= %s at time zone 'UTC'
            AND trade_date <= %s at time zone 'UTC'""", (market, daystart, dayend))
        r = c.fetchone()
        volume, spent = r
        if volume is None: volume = 0; spent = 0

        c.execute("""INSERT INTO trade_volumes (trade_volume_market_id, trade_volume_day, trade_volume_daily_volume, trade_volume_daily_spent)
            VALUES(%s, %s at time zone 'UTC', %s, %s)""", (market, daystart, volume, spent))

        daystart = daystart + datetime.timedelta(days=1)

for market in markets:
    c.execute("""SELECT settings_val FROM settings
        WHERE settings_var = %s
        """, ("last_dailyvolume_market"+str(market),))
    r = c.fetchone()
    if r is None:
        # Find day of last trade
        c.execute("""SELECT trade_date at time zone 'UTC' FROM trades
            WHERE trade_market_id = %s
            ORDER BY trade_date DESC LIMIT 1""", (market,))
        r = c.fetchone()
        if r is None: continue

        # Only store daily volumes for days before day of last trade with one hour room for error in system time
        # Calculate timestamp for 23:00:00 UTC day before last trade
        # If last trade is January 5th, 23:35:00 UTC, then return January 4th, 23:00:00 UTC
        # If last trade is January 5th, 00:35:00 UTC, then return January 4th, 23:00:00 UTC
        daybeforelasttrade = r[0] - datetime.timedelta(hours=r[0].hour+1, minutes=r[0].minute, seconds=r[0].second, microseconds=r[0].microsecond)
        daybeforelasttradets = int(time.mktime(daybeforelasttrade.timetuple()))

        # Find day of first trade
        c.execute("""SELECT trade_date at time zone 'UTC' FROM trades
            WHERE trade_market_id = %s
            ORDER BY trade_date LIMIT 1""", (market,))
        r = c.fetchone()
        if r is not None:
            # Wait until at least one trade exists before calculating daily volumes
            getDailyVolume(market, r[0], daybeforelasttrade)
            c.execute("""INSERT INTO settings (settings_var, settings_val) VALUES (%s, %s)""",
                ("last_dailyvolume_market"+str(market), daybeforelasttradets))
    else:
        lastdailyvol = int(r[0])

        # Find day of last trade
        c.execute("""SELECT trade_date at time zone 'UTC' FROM trades
            WHERE trade_market_id = %s
            ORDER BY trade_date DESC LIMIT 1""", (market,))
        r = c.fetchone()
        if r is None: continue # In case table was emptied?

        daybeforelasttrade = r[0] - datetime.timedelta(hours=r[0].hour+1, minutes=r[0].minute, seconds=r[0].second, microseconds=r[0].microsecond)
        daybeforelasttradets = int(time.mktime(daybeforelasttrade.timetuple()))

        if lastdailyvol < daybeforelasttradets:
            c.execute("""SELECT trade_date at time zone 'UTC' FROM trades
                WHERE trade_market_id = %s AND trade_date > to_timestamp(%s)
                ORDER BY trade_date LIMIT 1""", (market,lastdailyvol))
            r = c.fetchone()
            if r is not None:
                # Wait until at least one trade exists before calculating daily volumes
                getDailyVolume(market, r[0], daybeforelasttrade)
                c.execute("""UPDATE settings SET settings_val = %s WHERE settings_var = %s""",
                    (daybeforelasttradets, "last_dailyvolume_market"+str(market)))

db.commit()
c.close()
db.close()