#!/usr/bin/python
import calendar, json, os, psycopg2, stat, time, tempfile

def writeJSONToFile(json_data, file):
    f = tempfile.NamedTemporaryFile(delete=False, dir='tmp/')
    os.chmod(f.name,stat.S_IREAD | stat.S_IWRITE | stat.S_IRGRP | stat.S_IROTH)
    f.write(json.dumps(json_data))
    f.close()
    os.rename(f.name, file)

if os.path.exists('tmp/') == False: os.mkdir('tmp/')

while True:
    db = psycopg2.connect(database="bitcoinmarkets",user="bitcoinmarkets")
    c = db.cursor()
    c.execute("""SELECT trade_volume_market_id,
        trade_volume_day at time zone 'UTC',
        trade_volume_daily_volume,
        trade_volume_daily_spent
        FROM trade_volumes
        ORDER BY trade_volume_day""")
    daily_volumes = []
    for result in c.fetchall():
        (market_id,
        trade_volume_day,
        trade_volume_daily_volume,
        trade_volume_daily_spent) = result

        json_data = {
            'mid':market_id,
            'day':calendar.timegm(trade_volume_day.timetuple()),
            'volume':str(trade_volume_daily_volume),
            'spent':str(trade_volume_daily_spent)
        }
        daily_volumes.append(json_data)

    writeJSONToFile(daily_volumes, 'data/daily_volumes')
    c.close
    db.close()
    time.sleep(360)