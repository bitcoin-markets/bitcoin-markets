#!/usr/bin/python
import datetime, json, locale, os, random, socket, threading, time

locale.setlocale(locale.LC_ALL, 'en_US')

class message:
    def __str__(self):
        return "<message "+str(self.prefix)+" "+str(self.command)+" "+str(self.params)+" "+str(self.trailing)+">"

class irc_stream:
    def __init__(self,s):
        self.socket = s
        self.buffer = ""
    def read_messages(self):
        try: buffer = self.socket.recv(8192)
        except socket.timeout: return []
        messages = []
        if buffer == '':
            #raise socket.error
            m = message()
            m.command = 'DISCONNECTED'
            messages.append(m)
            return messages
        self.buffer += buffer
        while "\r\n" in self.buffer:
            i = self.buffer.find("\r\n")
            line = self.buffer[:i]
            self.buffer = self.buffer[i+2:]
            m = message()
            m.line = line
            if line[0] == ":":
                i = line.find(" ")
                m.prefix = line[1:i]
                line = line[i+1:]
            else:
                m.prefix = None
            i = line.find(" ")
            m.command = line[:i]
            line = line[i+1:]
            m.params = []
            m.trailing = None
            while True:
                if len(line) == 0: break
                elif line[0] == ":":
                    m.trailing = line[1:]
                    break
                else:
                    i = line.find(" ")
                    if i == -1:
                        m.params.append(line)
                        break
                    else:
                        m.params.append(line[:i])
                        line = line[i+1:]
                time.sleep(0.05)
            messages.append(m)
        return messages

class bitcoinmarkets_bot(threading.Thread):
    def __init__(self):
        super(bitcoinmarkets_bot, self).__init__()
        self.socket = socket.socket()
        self.socket.settimeout(1)
        self.send_queue = []
    def clear_queue(self):
        for line in self.send_queue:
            try:
                self.socket.sendall(line+'\r\n')
            except socket.error:
                self.activemarkets = False
                self.activetickers = False
                time.sleep(10)
                self.reconnect()
                return
            time.sleep(1)
        self.send_queue = []
    def connect(self):
        try: self.socket.connect((self.host,self.port))
        except socket.timeout, e: self.connect()
        self.reader = irc_stream(self.socket)
        self.send_queue.append("USER btcm 0 * :btcm")
        self.send_queue.append("NICK "+self.random_string())
        #self.send_queue.append("PRIVMSG nickserv :IDENTIFY "+freenodepw)
        self.clear_queue()
        while True:
            scanForNewTickers()
            scanForNewTrades()
            try:
                for m in self.reader.read_messages():
                    #http://www.networksorcery.com/enp/protocol/irc.htm
                    #if m.command == "001": pass #RPL_WELCOME
                    #elif m.command == "002": pass #RPL_YOURHOST
                    #elif m.command == "003": pass #RPL_CREATED
                    #elif m.command == "004": pass #RPL_MYINFO
                    #elif m.command == "005": pass #RPL_BOUNCE
                    #elif m.command == "250": pass
                    #elif m.command == "251": pass #RPL_LUSERCLIENT
                    #elif m.command == "252": pass #RPL_LUSEROP
                    #elif m.command == "253": pass #RPL_LUSERUNKNOWN
                    #elif m.command == "254": pass #RPL_LUSERCHANNELS
                    #elif m.command == "255": pass #RPL_LUSERME
                    #elif m.command == "265": pass
                    #elif m.command == "266": pass
                    if m.command == "353": #RPL_NAMREPLY
                        if m.params[2] == "#bitcoin-markets": self.activemarkets = True
                        if m.params[2] == "#bitcoin-tickers": self.activetickers = True
                    #elif m.command == "372": pass #RPL_MOTD
                    #elif m.command == "375": pass #RPL_MOTDSTART
                    #elif m.command == "376": pass #RPL_ENDOFMOTD
                    elif m.command == "433": #ERR_NICKNAMEINUSE
                        self.send_queue.append("PRIVMSG nickserv :GHOST "+self.nick+" "+freenodepw)
                        self.send_queue.append('NICK '+self.nick)
                        time.sleep(1)
                    elif m.command == "DISCONNECTED":
                        self.connect()
                    elif m.command == "MODE":
                        self.send_queue.append('NICK '+self.nick)
                    elif m.command == "NICK":
                        if m.trailing == self.nick:
                            self.send_queue.append("MODE "+self.nick+" +D")
                            self.send_queue.append("JOIN #bitcoin-markets,#bitcoin-markets-eur,#bitcoin-markets-usd,#bitcoin-markets-sll,#bitcoin-markets-aud,#bitcoin-markets-hkd,#bitcoin-markets-cad,#bitcoin-markets-gbp,#bitcoin-markets-bgn,#bitcoin-markets-brl,#bitcoin-markets-chf,#bitcoin-markets-inr,#bitcoin-markets-jpy,#bitcoin-markets-nzd,#bitcoin-markets-pln,#bitcoin-markets-rub,#bitcoin-markets-sek,#bitcoin-markets-sgd,#bitcoin-markets-thb,#bitcoin-markets-zar")
                            self.send_queue.append("JOIN #bitcoin-tickers,#bitcoin-tickers-eur,#bitcoin-tickers-usd,#bitcoin-tickers-sll,#bitcoin-tickers-aud,#bitcoin-tickers-hkd,#bitcoin-tickers-cad,#bitcoin-tickers-gbp,#bitcoin-tickers-bgn,#bitcoin-tickers-brl,#bitcoin-tickers-chf,#bitcoin-tickers-inr,#bitcoin-tickers-jpy,#bitcoin-tickers-nzd,#bitcoin-tickers-pln,#bitcoin-tickers-rub,#bitcoin-tickers-sek,#bitcoin-tickers-sgd,#bitcoin-tickers-thb,#bitcoin-tickers-zar")
                    elif m.command == "PING":
                        self.send_queue.append('PONG '+m.trailing)
                    else: pass
                time.sleep(0.01)
            except socket.timeout:
                pass

            if self.activemarkets == True:
                global tradeFiles
                newfile = self.random_string(32)
                if len(tradeFiles) > 0:
                    try: file = tradeFiles.pop(0)
                    except IndexError: continue
                    if os.path.exists('/dev/shm/btcm/irc/trades/'+file):
                        try: os.rename('/dev/shm/btcm/irc/trades/'+file, '/dev/shm/btcm/irc/trades/'+newfile)
                        except OSError: continue
                        try: json_data = json.load(open('/dev/shm/btcm/irc/trades/'+newfile))
                        except IOError: continue
                        try: os.unlink('/dev/shm/btcm/irc/trades/'+newfile)
                        except OSError: continue
                        strTime = datetime.datetime.utcfromtimestamp(float(json_data['date'])).strftime('%b%d %H:%M:%S')
                        strMarket = json_data['market'].ljust(12)
                        strQuantity = locale.format("%.*f", (8, float(json_data['quantity'])), True).rjust(14)
                        strPrice = formatPrice(locale.format("%.*f", (8, float(json_data['price'])), True))
                        strCurrency = json_data['currency']
                        strLine1 = strTime + ' \x02' + strMarket + '\x02 ' + strQuantity + ' @\x02' + strPrice + '\x02' + strCurrency
                        strLine2 = strTime + ' \x02' + strMarket + '\x02 ' + strQuantity + ' @\x02' + strPrice + '\x02'
                        self.send_queue.append("PRIVMSG #bitcoin-markets-"+json_data['currency']+" :"+unicode(strLine2))
                        self.send_queue.append("PRIVMSG #bitcoin-markets :"+unicode(strLine1))

            self.clear_queue()

            if self.activetickers == True:
                global tickerFiles
                newfile = self.random_string(32)
                if len(tickerFiles) > 0:
                    try: file = tickerFiles.pop(0)
                    except IndexError: continue
                    if os.path.exists('/dev/shm/btcm/irc/tickers/'+file):
                        try: os.rename('/dev/shm/btcm/irc/tickers/'+file, '/dev/shm/btcm/irc/tickers/'+newfile)
                        except OSError: continue
                        try: json_data = json.load(open('/dev/shm/btcm/irc/tickers/'+newfile))
                        except IOError: continue
                        try: os.unlink('/dev/shm/btcm/irc/tickers/'+newfile)
                        except OSError: continue
                        if json_data['avg'] == '':
                            strAvg = ''.rjust(15)
                        else:
                            strAvg = ' avg:\x02' + locale.format("%.*f", (4, float(json_data['avg'])), True).rjust(10) + '\x02'
                        if json_data['buy'] == '':
                            strBuy = ''.rjust(15)
                        else:
                            strBuy = ' bid:\x02' + locale.format("%.*f", (4, float(json_data['buy'])), True).rjust(10) + '\x02'
                        if json_data['dayhigh'] == '':
                            strHigh = ''.rjust(16)
                        else:
                            strHigh = ' high:\x02' + locale.format("%.*f", (4, float(json_data['dayhigh'])), True).rjust(10) + '\x02'
                        if json_data['daylow'] == '':
                            strLow = ''.rjust(15)
                        else:
                            strLow = ' low:\x02' + locale.format("%.*f", (4, float(json_data['daylow'])), True).rjust(10) + '\x02'
                        if json_data['last'] == '':
                            strLast = ''.rjust(16)
                        else:
                            strLast = ' last:\x02' + locale.format("%.*f", (4, float(json_data['last'])), True).rjust(10) + '\x02'
                        strExchange = json_data['exchange'].ljust(12)
                        strMarket = json_data['market'].ljust(13)
                        if json_data['sell'] == '':
                            strSell = ''.rjust(16)
                        else:
                            strSell = ' ask:\x02' + locale.format("%.*f", (4, float(json_data['sell'])), True).rjust(10) + '\x02'
                        if json_data['vol'] == '':
                            strVol = ''.rjust(16)
                        else:
                            strVol = ' vol:\x02' + locale.format("%.*f", (4, float(json_data['vol'])), True).rjust(12) + '\x02'
                        if json_data['vwap'] == '':
                            strVwap = ''.rjust(16)
                        else:
                            strVwap = ' vwap:\x02' + locale.format("%.*f", (4, float(json_data['vwap'])), True).rjust(10) + '\x02'
                        strLine1 = strMarket + strBuy + strSell + strLow + strHigh + strLast + strAvg + strVol + strVwap
                        strLine2 = strExchange + strBuy + strSell + strLow + strHigh + strLast + strAvg + strVol + strVwap
                        self.send_queue.append("PRIVMSG #bitcoin-tickers-"+json_data['currency']+" :"+unicode(strLine2))
                        self.send_queue.append("PRIVMSG #bitcoin-tickers :"+unicode(strLine1))

            self.clear_queue()
    def random_string(self,length=12):
        return ''.join([random.choice("abcdefghijklmnopqrstuvwxyz") for x in xrange(length)])
    def reconnect(self):
        self.socket = socket.socket()
        self.socket.settimeout(10)
        self.send_queue = []
        self.connect()
    def run(self):
        self.connect()

def formatPrice(num):
    # Minimum two decimal places
    num = num.rstrip("0")
    if num.find(".") + 3 > len(num): num = num + "0" * (num.find(".") + 3 - len(num))
    return " " * (8 - num.find(".")) + num + " " * (9 - (len(num) - num.find(".")))

def scanForNewTrades():
    global tradeFiles
    if len(tradeFiles) <= 1000:
        files = os.listdir('/dev/shm/btcm/irc/trades/')
        files.sort()
        for file in files:
            if os.path.exists('/dev/shm/btcm/irc/trades/'+file) and file not in tradeFiles:
                tradeFiles.append(file)
                tradeFiles.sort()

def scanForNewTickers():
    global tickerFiles
    if len(tickerFiles) <= 1000:
        files = os.listdir('/dev/shm/btcm/irc/tickers/')
        files.sort()
        for file in files:
            if os.path.exists('/dev/shm/btcm/irc/tickers/'+file) and file not in tickerFiles:
                tickerFiles.append(file)
                tickerFiles.sort()

nicks=['btcm0','btcm1','btcm2','btcm3','btcm4','btcm5','btcm6','btcm7','btcm8','btcm9','btcmA','btcmB','btcmC','btcmD','btcmE','btcmF','btcmG','btcmH','btcmI','btcmJ','btcmK','btcmL','btcmM','btcmN','btcmO','btcmP','btcmQ','btcmR','btcmS','btcmT','btcmU','btcmV','btcmW','btcmX','btcmY','btcmZ']
freenodepw = open('priv/freenode').read().strip()

bots = set()

tickerFiles = []
tradeFiles = []

threading.stack_size(32768)

for nick in nicks:
    bot = bitcoinmarkets_bot()
    bot.host = "irc.freenode.net"
    bot.port = 6667
    bot.nick = nick
    bot.activemarkets = False
    bot.activetickers = False
    bot.daemon = True
    bot.start()
    bots.add(bot)
    time.sleep(10)

while True: time.sleep(0.01)