#!/usr/bin/python
import calendar, json, os, psycopg2, stat, time, tempfile

def writeJSONToFile(json_data, file):
    f = tempfile.NamedTemporaryFile(delete=False, dir='tmp/')
    os.chmod(f.name,stat.S_IREAD | stat.S_IWRITE | stat.S_IRGRP | stat.S_IROTH)
    f.write(json.dumps(json_data))
    f.close()
    os.rename(f.name, file)

if os.path.exists('tmp/') == False: os.mkdir('tmp/')

while True:
    db = psycopg2.connect(database="bitcoinmarkets",user="bitcoinmarkets")
    c = db.cursor()
    c.execute("""SELECT market_id,
        trade_todayvolumedate at time zone 'UTC',
        trade_todayvolumequantity,
        trade_todayvolumeprice,
        trade_todayvolumepriceavg,
        trade_dailyvolumequantity,
        trade_dailyvolumeprice,
        trade_dailyvolumepriceavg,
        trade_monthlyvolumequantity,
        trade_monthlyvolumeprice,
        (trade_latest).trade_id AS trade_latest_trade_id,
        (trade_latest).trade_date AS trade_latest_trade_date,
        (trade_latest).trade_price AS trade_latest_trade_price,
        (trade_latest).trade_quantity AS trade_latest_trade_quantity,
        (trade_previousclose).trade_id AS trade_previousclose_trade_id,
        (trade_previousclose).trade_date AS trade_previousclose_trade_date,
        (trade_previousclose).trade_price AS trade_previousclose_trade_price,
        (trade_previousclose).trade_quantity AS trade_previousclose_trade_quantity,
        (trade_open).trade_id AS trade_open_trade_id,
        (trade_open).trade_date AS trade_open_trade_date,
        (trade_open).trade_price AS trade_open_trade_price,
        (trade_open).trade_quantity AS trade_open_trade_quantity,
        (trade_ticker).depth_highbid AS trade_highbid,
        (trade_ticker).depth_lowask AS trade_lowask,
        (trade_dayhigh).trade_price AS trade_dayhigh,
        (trade_daylow).trade_price AS trade_daylow
        FROM (
        	SELECT m.market_id AS market_id,
        	(
        		SELECT t FROM trades t
        		WHERE t.trade_market_id=m.market_id
        			--AND t.trade_reversed=FALSE
        			AND t.trade_primary=TRUE
        		ORDER BY trade_date DESC LIMIT 1
        	) as trade_latest,
        	(
        		SELECT t FROM trades t
        		WHERE t.trade_market_id=m.market_id
        			AND t.trade_date<date_trunc('day', now() at time zone 'UTC') at time zone 'UTC'
        			--AND t.trade_reversed=FALSE
        			AND t.trade_primary=TRUE
        		ORDER BY trade_date DESC LIMIT 1
        	) as trade_previousclose,
        	(
        		SELECT t FROM trades t
        		WHERE t.trade_market_id=m.market_id
        			AND t.trade_date>(
        				SELECT t.trade_date FROM trades t
        				WHERE t.trade_market_id=m.market_id AND t.trade_date < date_trunc('day', now() at time zone 'UTC') at time zone 'UTC'
        				ORDER BY t.trade_date DESC LIMIT 1
        			)
        			--AND t.trade_reversed=FALSE
        			AND t.trade_primary=TRUE
        		ORDER BY t.trade_date ASC LIMIT 1
        	) as trade_open,
        	(
        		SELECT date_trunc('day', now() at time zone 'UTC') at time zone 'UTC'
        	) as trade_todayvolumedate,
        	(
        		SELECT SUM(t.trade_quantity) FROM trades t
        		WHERE t.trade_market_id=m.market_id
        			AND t.trade_date >= date_trunc('day', now() at time zone 'UTC') at time zone 'UTC'
        			--AND t.trade_reversed=FALSE
        			AND t.trade_primary=TRUE
        	) as trade_todayvolumequantity,
        	(
        		SELECT SUM(t.trade_quantity * t.trade_price) FROM trades t
        		WHERE t.trade_market_id=m.market_id
        			AND t.trade_date >= date_trunc('day', now() at time zone 'UTC') at time zone 'UTC'
        			--AND t.trade_reversed=FALSE
        			AND t.trade_primary=TRUE
        	) as trade_todayvolumeprice,
        	(
        		SELECT AVG(t.trade_price) FROM trades t
        		WHERE t.trade_market_id=m.market_id
        			AND t.trade_date >= date_trunc('day', now() at time zone 'UTC') at time zone 'UTC'
        			--AND t.trade_reversed=FALSE
        			AND t.trade_primary=TRUE
        	) as trade_todayvolumepriceavg,
        	(
        		SELECT SUM(t.trade_quantity) FROM trades t
        		WHERE t.trade_market_id=m.market_id
        			AND t.trade_date >= (now() - interval '24 hours')
        			--AND t.trade_reversed=FALSE
        			AND t.trade_primary=TRUE
        	) as trade_dailyvolumequantity,
        	(
        		SELECT SUM(t.trade_quantity * t.trade_price) FROM trades t
        		WHERE t.trade_market_id=m.market_id
        			AND t.trade_date >= (now() - interval '24 hours')
        			--AND t.trade_reversed=FALSE
        			AND t.trade_primary=TRUE
        	) as trade_dailyvolumeprice,
        	(
        		SELECT AVG(t.trade_price) FROM trades t
        		WHERE t.trade_market_id=m.market_id
        			AND t.trade_date >= (now() - interval '24 hours')
        			--AND t.trade_reversed=FALSE
        			AND t.trade_primary=TRUE
        	) as trade_dailyvolumepriceavg,
        	(
        		SELECT
        			(SELECT SUM(tv.trade_volume_daily_volume) FROM trade_volumes tv
        			WHERE tv.trade_volume_market_id=m.market_id
        			AND tv.trade_volume_day >= date_trunc('day', (now() - interval '30 days') at time zone 'UTC') at time zone 'UTC'
        			AND tv.trade_volume_day < date_trunc('day', (now() - interval '1 day') at time zone 'UTC') at time zone 'UTC'
        			--AND tv.trade_reversed=FALSE
        			AND tv.trade_primary=TRUE)
        			+
        			(SELECT SUM(t.trade_quantity) FROM trades t
        			WHERE t.trade_market_id=m.market_id
        			AND (
        				(
        					t.trade_date >= (now() - interval '30 days')
        					AND t.trade_date < date_trunc('day', (now() - interval '29 days') at time zone 'UTC') at time zone 'UTC'
        				)
        				OR
        				t.trade_date >= date_trunc('day', (now() - interval '1 day') at time zone 'UTC') at time zone 'UTC'
        			)
        			--AND tv.trade_reversed=FALSE
        			AND tv.trade_primary=TRUE)
        	) as trade_monthlyvolumequantity,
        	(
        		SELECT
        			(SELECT SUM(tv.trade_volume_daily_spent) FROM trade_volumes tv
        			WHERE tv.trade_volume_market_id=m.market_id
        			AND tv.trade_volume_day >= date_trunc('day', (now() - interval '30 days') at time zone 'UTC') at time zone 'UTC'
        			AND tv.trade_volume_day < date_trunc('day', (now() - interval '1 day') at time zone 'UTC') at time zone 'UTC'
        			--AND tv.trade_reversed=FALSE
        			AND tv.trade_primary=TRUE)
        			+
        			(SELECT SUM(t.trade_quantity * t.trade_price) FROM trades t
        			WHERE t.trade_market_id=m.market_id
        			AND (
        				(
        					t.trade_date >= (now() - interval '30 days')
        					AND t.trade_date < date_trunc('day', (now() - interval '29 days') at time zone 'UTC') at time zone 'UTC'
        				)
        				OR
        				t.trade_date >= date_trunc('day', (now() - interval '1 day') at time zone 'UTC') at time zone 'UTC'
        			)
        			--AND tv.trade_reversed=FALSE
        			AND tv.trade_primary=TRUE)
        	) as trade_monthlyvolumeprice,
        	(
        		SELECT d FROM depth d WHERE d.depth_market_id=m.market_id
        			--AND t.trade_reversed=FALSE
        			AND t.trade_primary=TRUE
        		ORDER BY depth_timestamp DESC LIMIT 1
        	) as trade_ticker,
        	(
        		SELECT t FROM trades t
        		WHERE t.trade_market_id=m.market_id
        			AND t.trade_date >= date_trunc('day', now() at time zone 'UTC') at time zone 'UTC'
        			--AND t.trade_reversed=FALSE
        			AND t.trade_primary=TRUE
        		ORDER BY t.trade_price ASC LIMIT 1
        	) as trade_daylow,
        	(
        		SELECT t FROM trades t
        		WHERE t.trade_market_id=m.market_id
        			AND t.trade_date >= date_trunc('day', now() at time zone 'UTC') at time zone 'UTC'
        			--AND t.trade_reversed=FALSE
        			AND t.trade_primary=TRUE
        		ORDER BY t.trade_price DESC LIMIT 1
        	) as trade_dayhigh
        	FROM markets m, currencies c
        	WHERE market_currency_id=currency_id
        ) s
        -- This is too slow
        --ORDER BY trade_monthlyvolumequantity DESC NULLS LAST""")
    trades = []
    for result in c.fetchall():
        (market_id,
        trade_todayvolumedate,
        trade_todayvolumequantity,
        trade_todayvolumeprice,
        trade_todayvolumepriceavg,
        trade_dailyvolumequantity,
        trade_dailyvolumeprice,
        trade_dailyvolumepriceavg,
        trade_monthlyvolumequantity,
        trade_monthlyvolumeprice,
        trade_latest_trade_id,
        trade_latest_trade_date,
        trade_latest_trade_price,
        trade_latest_trade_quantity,
        trade_previousclose_trade_id,
        trade_previousclose_trade_date,
        trade_previousclose_trade_price,
        trade_previousclose_trade_quantity,
        trade_open_trade_id,
        trade_open_trade_date,
        trade_open_trade_price,
        trade_open_trade_quantity,
        trade_highbid,
        trade_lowask,
        trade_dayhigh,
        trade_daylow) = result

        json_data = {'mid':market_id}
        if trade_todayvolumequantity is not None:
            json_data['todayvolumedate'] = calendar.timegm(trade_todayvolumedate.timetuple());
            json_data['todayvolumequantity'] = str(trade_todayvolumequantity);
            json_data['todayvolumeprice'] = str(trade_todayvolumeprice);
            json_data['todayvolumepriceavg'] = str(trade_todayvolumepriceavg);
        else:
            json_data['todayvolumequantity'] = '0';
            json_data['todayvolumeprice'] = '';
            json_data['todayvolumepriceavg'] = '';
        if trade_dailyvolumequantity is not None:
            json_data['dailyvolumequantity'] = str(trade_dailyvolumequantity);
            json_data['dailyvolumeprice'] = str(trade_dailyvolumeprice);
            json_data['dailyvolumepriceavg'] = str(trade_dailyvolumepriceavg);
        else:
            json_data['dailyvolumequantity'] = '0';
            json_data['dailyvolumeprice'] = '';
            json_data['dailyvolumepriceavg'] = '';
        if trade_monthlyvolumequantity is not None:
            json_data['monthlyvolumequantity'] = str(trade_monthlyvolumequantity);
            json_data['monthlyvolumeprice'] = str(trade_monthlyvolumeprice);
        else:
            json_data['monthlyvolumequantity'] = '0';
            json_data['monthlyvolumeprice'] = '';
        if trade_latest_trade_id is not None:
            json_data['latest_trade_id'] = trade_latest_trade_id
            json_data['latest_trade_date'] = time.mktime(trade_latest_trade_date.timetuple())
            json_data['latest_trade_price'] = str(trade_latest_trade_price)
            json_data['latest_trade_quantity'] = str(trade_latest_trade_quantity)
        if trade_previousclose_trade_id is not None:
            json_data['previousclose_trade_id'] = trade_previousclose_trade_id
            json_data['previousclose_trade_date'] = time.mktime(trade_previousclose_trade_date.timetuple())
            json_data['previousclose_trade_price'] = str(trade_previousclose_trade_price)
            json_data['previousclose_trade_quantity'] = str(trade_previousclose_trade_quantity)
        if trade_open_trade_id is not None:
            json_data['open_trade_id'] = trade_open_trade_id
            json_data['open_trade_date'] = time.mktime(trade_open_trade_date.timetuple())
            json_data['open_trade_price'] = str(trade_open_trade_price)
            json_data['open_trade_quantity'] = str(trade_open_trade_quantity)
        if trade_highbid is not None:
            json_data['highbid'] = str(trade_highbid)
        if trade_lowask is not None:
            json_data['lowask'] = str(trade_lowask)
        if trade_dayhigh is not None:
            json_data['dayhigh'] = str(trade_dayhigh)
        if trade_daylow is not None:
            json_data['daylow'] = str(trade_daylow)

        trades.append(json_data)

    writeJSONToFile(trades, 'data/trades')
    c.close()
    db.close()
    time.sleep(1)