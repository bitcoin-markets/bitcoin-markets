--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: -
--

CREATE OR REPLACE PROCEDURAL LANGUAGE plpgsql;


SET search_path = public, pg_catalog;

--
-- Name: market_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE market_type AS ENUM (
    'broker',
    'escrow',
    'exchange'
);


--
-- Name: currencies_currency_id_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE currencies_currency_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: currencies; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE currencies (
    currency_id smallint DEFAULT nextval('currencies_currency_id_sequence'::regclass) NOT NULL,
    currency_abbreviation character varying(45) NOT NULL,
    currency_name character varying(45) NOT NULL
);


--
-- Name: depth; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE depth (
    depth_id bigint NOT NULL,
    depth_market_id smallint NOT NULL,
    depth_highbid numeric(32,12),
    depth_lowask numeric(32,12),
    depth_timestamp timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: depth_depth_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE depth_depth_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: depth_depth_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE depth_depth_id_seq OWNED BY depth.depth_id;


--
-- Name: exchanges_exchange_id_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE exchanges_exchange_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: exchanges; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE exchanges (
    exchange_id smallint DEFAULT nextval('exchanges_exchange_id_sequence'::regclass) NOT NULL,
    exchange_name character varying(100),
    exchange_website character varying NOT NULL,
    exchange_email character varying NOT NULL,
    exchange_freenode character varying(49),
    exchange_phone character varying(20),
    exchange_address character varying,
    exchange_devname character varying(20),
    exchange_bitcointalk integer
);


--
-- Name: COLUMN exchanges.exchange_freenode; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN exchanges.exchange_freenode IS 'Freenode channel length restricted to 49 characters';


--
-- Name: markets_market_id_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE markets_market_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: markets; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE markets (
    market_id smallint DEFAULT nextval('markets_market_id_sequence'::regclass) NOT NULL,
    market_symbol character varying(16) NOT NULL,
    market_website character varying NOT NULL,
    market_data_ticker character varying(128),
    market_data_depth character varying(128),
    market_data_trades character varying(128),
    market_currency_id smallint NOT NULL,
    market_data_trades_all character varying(128),
    market_data_depth_all character varying(128),
    market_active boolean DEFAULT true NOT NULL,
    market_exchange_id smallint,
    market_transactionfee character varying,
    market_data_trades_canceled character varying(128),
    market_type market_type DEFAULT 'exchange'::market_type NOT NULL
);


--
-- Name: settings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE settings (
    settings_var character varying NOT NULL,
    settings_val character varying NOT NULL
);


--
-- Name: tickers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tickers (
    ticker_market_id smallint,
    ticker_timestamp timestamp with time zone DEFAULT now(),
    ticker_high numeric,
    ticker_low numeric,
    ticker_avg numeric,
    ticker_vwap numeric,
    ticker_vol numeric,
    ticker_last numeric,
    ticker_buy numeric,
    ticker_sell numeric
);


--
-- Name: trade_volumes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE trade_volumes (
    trade_volume_market_id smallint NOT NULL,
    trade_volume_day timestamp with time zone NOT NULL,
    trade_volume_daily_volume numeric(32,12) NOT NULL,
    trade_volume_daily_spent numeric(32,12) NOT NULL
);


--
-- Name: trades; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE trades (
    trade_market_id smallint,
    trade_id bigint,
    trade_date timestamp with time zone NOT NULL,
    trade_price numeric(32,12) NOT NULL,
    trade_quantity numeric(32,12) NOT NULL,
    trade_saved timestamp with time zone DEFAULT now() NOT NULL,
    trade_reversed boolean DEFAULT false NOT NULL,
    trade_primary boolean DEFAULT true NOT NULL
);


--
-- Name: depth_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE depth ALTER COLUMN depth_id SET DEFAULT nextval('depth_depth_id_seq'::regclass);


--
-- Name: currencies_currency_abbreviation_unique; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY currencies
    ADD CONSTRAINT currencies_currency_abbreviation_unique UNIQUE (currency_abbreviation);


--
-- Name: currencies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY currencies
    ADD CONSTRAINT currencies_pkey PRIMARY KEY (currency_id);


--
-- Name: depth_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY depth
    ADD CONSTRAINT depth_pkey PRIMARY KEY (depth_id);


--
-- Name: exchanges_exchange_devname_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY exchanges
    ADD CONSTRAINT exchanges_exchange_devname_key UNIQUE (exchange_devname);


--
-- Name: exchanges_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY exchanges
    ADD CONSTRAINT exchanges_pkey PRIMARY KEY (exchange_id);


--
-- Name: markets_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY markets
    ADD CONSTRAINT markets_pkey PRIMARY KEY (market_id);


--
-- Name: settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (settings_var);


--
-- Name: trade_volumes_market_id_day_unique; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY trade_volumes
    ADD CONSTRAINT trade_volumes_market_id_day_unique UNIQUE (trade_volume_market_id, trade_volume_day);


--
-- Name: depth_depth_market_id_depth_timestamp_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX depth_depth_market_id_depth_timestamp_idx ON depth USING btree (depth_market_id, depth_timestamp);


--
-- Name: depth_depth_timestamp_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX depth_depth_timestamp_idx ON depth USING btree (depth_timestamp);


--
-- Name: markets_market_currency_id_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX markets_market_currency_id_idx ON markets USING btree (market_currency_id);


--
-- Name: tickers_ticker_timerstamp; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX tickers_ticker_timerstamp ON tickers USING btree (ticker_timestamp);


--
-- Name: trade_volumes_market_id_day_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX trade_volumes_market_id_day_idx ON trade_volumes USING btree (trade_volume_market_id, trade_volume_day);


--
-- Name: trades_trade_date_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX trades_trade_date_idx ON trades USING btree (trade_date);


--
-- Name: trades_trade_market_id_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX trades_trade_market_id_idx ON trades USING btree (trade_market_id);


--
-- Name: trades_trade_market_id_trade_date_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX trades_trade_market_id_trade_date_idx ON trades USING btree (trade_market_id, trade_date);


--
-- Name: trades_trade_market_id_trade_id_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX trades_trade_market_id_trade_id_idx ON trades USING btree (trade_market_id, trade_id);


--
-- Name: trades_trade_price_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX trades_trade_price_idx ON trades USING btree (trade_price);


--
-- Name: markets_exchange_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY markets
    ADD CONSTRAINT markets_exchange_id_fkey FOREIGN KEY (market_exchange_id) REFERENCES exchanges(exchange_id);


--
-- Name: markets_market_currency_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY markets
    ADD CONSTRAINT markets_market_currency_id_fkey FOREIGN KEY (market_currency_id) REFERENCES currencies(currency_id);


--
-- Name: tickers_ticker_market_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tickers
    ADD CONSTRAINT tickers_ticker_market_id_fkey FOREIGN KEY (ticker_market_id) REFERENCES markets(market_id);


--
-- Name: trade_volumes_market_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trade_volumes
    ADD CONSTRAINT trade_volumes_market_id_fkey FOREIGN KEY (trade_volume_market_id) REFERENCES markets(market_id);


--
-- Name: trades_trade_market_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trades
    ADD CONSTRAINT trades_trade_market_id_fkey FOREIGN KEY (trade_market_id) REFERENCES markets(market_id);


--
-- PostgreSQL database dump complete
--

